import sys
import os
import glob
import shutil
import re
import subprocess

path_exec=os.path.dirname(  __file__ )
if not path_exec :
    path_exec='.'
sys.path.append(path_exec + os.sep + 'gess' + os.sep + 'common')

def create_mo_files():
    data_files = []
    localedir = os.path.join('gess', 'i18n')
    po_dirs = [os.path.join(localedir, l, 'LC_MESSAGES') for l in next(os.walk(localedir))[1]]
    for d in po_dirs:
        mo_files = []
        po_files = [f for f in next(os.walk(d))[2] if os.path.splitext(f)[1] == '.po']
        for po_file in po_files:
            filename, extension = os.path.splitext(po_file)
            mo_file = filename + '.mo'
            try:
                msgfmt_cmd = 'msgfmt {} -o {}'.format(os.path.join(d, po_file), os.path.join(d, mo_file))
                subprocess.call(msgfmt_cmd, shell=True)
            except:
                pass
            mo_files.append(os.path.join(d, mo_file))
        data_files.append((d, mo_files))
    return data_files

versions=[]
# with open(path_exec + os.sep + 'gess' + os.sep + "Changelog.txt","r") as fd :
with open(path_exec + os.sep + "CHANGELOG","r") as fd :
    lines= fd.readlines()
for line in lines:
    if re.match(r'^ V\d+\.\d+.\d+', line) :
        versions.append(line)


# ------------------------------------------------------------------------------
# clean build directories and files
def clean_build(dirlist=None,filelist=None):
    if dirlist is not None :
        for  dirglb in dirlist :
             for  dir in glob.glob( dirglb ):
                print("* clean:",dir )
                shutil.rmtree(dir,ignore_errors=True)
    if filelist is not None :
        for  fileglb in filelist :
            for  fic in glob.glob( fileglb ) :
                print("* clean:",fic )
                try:
                    os.unlink(fic)
                except:
                    pass
# ------------------------------------------------------------------------------      
def package_setuptools():
    from setuptools import setup, find_packages
    long_description = """ gess (GenerateSirilScript)
    
gess est une suite de fonctions Python qui va permettre d’automatiser les étapes 
de prétraitement , d’alignement et empilement de sessions dans Siril. Ça ressemble 
fort aux scripts Siril et aussi pas mal a Sirilic, avec quelques différences tout 
de même :
    - Il permet de modifier a la volée votre process habituel si, par exemple, 
      vous n’avez pas fait de flats dans votre dernière session. Plus besoin 
      d’avoir un script pour chaque situation (Nodark, Noflat, NodarkNoflat, et 
      rebelote avec l’extraction Ha/OIII etc…)
    - Pour les utilisateurs de camera avec refroidissement, les darks et les 
      offsets sont choisis automatiquement dans une bibliothèque. Les infos 
      utiles sont lues dans les headers des brutes et des flats pour trouver 
      les masters correspondants.
     - Il permet aussi de regrouper/aligner/empiler des sessions sur plusieurs 
       nuits.
    """

    setup(
        name = "gess",
        version = versions[0].strip().split()[0] ,
        author="cissou8",
        author_email="cissou8@gmail.com",
        license='GPL-v3',
        description='gess (Generate Siril Script) is an automation tool for Siril, based on pysiril module.',
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/cissou8/gess.git",
        packages=find_packages(),
        python_requires='>=3.6',
        classifiers=[
            "Programming Language :: Python :: 3",
            "Development Status :: 5 - Production/Stable",
            "Programming Language :: Python :: 3.6",
            "Topic :: Scientific/Engineering :: Astronomy",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
        ],
        install_requires=['pysiril @ https://gitlab.com/free-astro/pysiril/-/archive/master/pysiril-master.tar.gz',
        'pysimplegui','astropy','pyexiv2'],
        entry_points={
              'console_scripts': [ 
                    'gess=gess.main:main'
                  ],
          },
        include_package_data=True, # Needed to include files listed in MANIFEST.in into wheels
        data_files=create_mo_files(),
    )
    
# ------------------------------------------------------------------------------
if len(sys.argv ) == 1 :
    print( "Build Package:" )
    print( "    o python source package :" )
    print( "        > python setup.py sdist" )
    print( "")
    print( "    o python (*.whl) package :" )
    print( "        > python setup.py bdist_wheel " )
    print( "")
    print( "    o debian (*.deb) package :" )
    print( "        > python setup.py bdist_deb" )
    print( "")
    exit( 0 )
    
clean_build( [ 'build','dist','deb_dist', '*egg-info' ], ['*tar.gz'] )
package_setuptools()
clean_build( [ 'build', '*egg-info' ], ['*tar.gz' ] )
    
