class DictX(dict):
    """A overload of dict class that accepts dot assignment

    :param dict: a dictionnary
    :type dict: dict
    """
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError as k:
            raise AttributeError(k)

    def __setattr__(self, key, value):
        self[key] = value

    def __copy__(self):
        return self