import os,sys
import json
from distutils.util import strtobool
from json.decoder import JSONDecodeError
import tkinter as tk
from tkinter import filedialog

from gess.common.i18n import _

from .DictX import DictX

#TODO add export diff method
class options(object):
    """ 
    Class to handle options passed to gessengine, gessloop and copyback.

    Default cfg file are stored in your user folder at ./gess/

    This class is called at initialization of the package to create default cfg files if none is present:

    - gess.cfg: the options for your typical worflow

    - loop.cfg: additional options to use gessloop module

    - copyback.cfg : additional options to use copyback module

    
    :param optiontype: the type of options, either 'gess', 'loop' or 'copyback', defaults to 'gess'
    :type optiontype: str, optional
    :param addcfgfile: an additional cfg file to read more options from on top of default values, defaults to None
    :type addcfgfile: str, optional
    :param dictcfg: a dictionnary to read more options from on top of default values, defaults to {}
    :type dictcfg: dict, optional
    :param updatedefault: Flag to update default cfg with the values passed, defaults to False
    :type updatedefault: bool, optional
    :param returnclean: Flag to return a clean version of the options attribute with all values to default, defaults to False
    :type returnclean: bool, optional

    """

    def __init__(self,optiontype='gess',addcfgfile=None,dictcfg={},updatedefault=False,returnclean=False):
        """class constructor

        """
        try:
            updatedefault=strtobool(updatedefault)
        except:
            pass


        self.optiontype=optiontype
        self.options=DictX()

        if 'gess' in optiontype.lower():
            self.options.ext=           'fits'
            self.options.shottype=      'cfa'
            self.options.bitdepth=      32
            self.options.seqasfitseq=   False
            self.options.compression=   '' # new option - if '', keeps Siril current compression settings
            self.options.debug=         True
            self.options.sirilexe=      ''

            self.options.ppdark=        True
            self.options.ppflat=        True
            self.options.ppoffset=      True # new option
            self.options.pplight=       True
            self.options.dobkg=         False # new option
            self.options.doHO=          False
            self.options.doregister=    True
            self.options.dostack=       True
            self.options.lightsfmt=     '' #new option to parse results names

            self.options.lights=        _('lights')
            self.options.lightsfmt=     '' #new option to name lights sequence
            self.options.darks=         _('darks')
            self.options.darksfmt=      ''  
            self.options.flats=         _('flats')
            self.options.flatsfmt=      '' 
            self.options.biases=        _('biases')
            self.options.biasesfmt=     ''
            self.options.darkflats=     _('darkflats')
            self.options.darkflatsfmt=  ''
            self.options.copymasters=   True

        elif 'loop' in optiontype.lower():  
            self.options.imagingfolder= ''
            self.options.datefmt=       '%Y-%m-%d'
            self.options.darks2lib=     ''
            self.options.flats2lib=     ''
            self.options.biases2lib=    ''
            self.options.darkflats2lib= ''
        
        elif 'copyback' in optiontype.lower():
            self.options.folderin= 'process'
            self.options.folderout=''
            self.options.levelup=1
            self.options.prefix=''
            self.options.suffix=''
            self.options.copyheader=False
            self.options.useoriginalFITSnames=False
            self.options.removepp=False
            self.options.onefolderperseq=True
            self.options.searchprefix='pp' # undocumented option
        else:
            print(_('{:s} is not a valid options type'))

        

        if os.name == 'nt':
            self.myhome=os.path.join(os.environ.get('USERPROFILE'),'gess')
        else:
            self.myhome=os.path.join(os.environ.get('HOME'),'gess')
            
        if not os.path.isdir(self.myhome):
            os.makedirs(self.myhome,exist_ok=True)

        self.defcfgfile=os.path.join(self.myhome,optiontype+'.cfg')

        if not returnclean:

            if not(os.path.isfile(self.defcfgfile)):
                with open(self.defcfgfile,'w') as fp:
                    json.dump(self.options,fp,sort_keys=False,indent=4)
        
        
            success=self.updateoptions_fromfile(self.defcfgfile)

            self.addcfgfile=''
            if not addcfgfile is None and not addcfgfile=='' :
                success=self.updateoptions_fromfile(addcfgfile)
                if success:
                    self.addcfgfile=addcfgfile
                else:
                    self.options={}

            self.dictcfg={}
            if len(dictcfg)>0:
                success=self.updateoptions_fromdict(dictcfg)
                if success:
                    self.dictcfg=dictcfg
                else:
                    self.options={}

            if updatedefault:
                with open(self.defcfgfile,'w') as fp:
                    json.dump(self.options,fp,sort_keys=False,indent=4)
    

    def updateoptions_fromdict(self,optdict):
        """method to update the options attribute with values from a dictionnary. Checks the type of the entries to make sure they match the types defined in the constructor.

        :param optdict: a dictionnary with values to update the options attribute
        :type optdict: dict
        :return: True if the update was successful
        :rtype: bool
        """
        for k,v in optdict.items():
            if k in self.options:
                if isinstance(self.options[k],bool):
                    try:
                        self.options[k]=bool(strtobool(str(v)))
                    except:
                        print(_('Could not convert your entry {:s} to boolean value').format(k))
                elif isinstance(self.options[k],float):
                    try:
                        self.options[k]=float(v)
                    except:
                        print(_('Could not convert your entry {:s} to float value').format(k))
                elif isinstance(self.options[k],int):
                    try:
                        self.options[k]=int(v)
                    except:
                        print(_('Could not convert your entry {:s} to integer value').format(k))
                else:
                    try:
                        self.options[k]=v.strip()
                    except:
                        self.options[k]=v
            else:
                print(_('Found option {0:s}').format(k))
                print(_('This option does not exists - ignoring'))

        if self.optiontype.lower()=='gess' and self.options.ppflat:
            self.options.ppoffset=True
        if self.optiontype.lower()=='gess' and not self.options.ppflat:
            self.options.ppoffset=False
        
        return True


    def updateoptions_fromfile(self,addcfg):
        """method to update the options attribute with values from a file.

        :param addcfg: the path to a cfg file. Can be either a full path or a filename. If filename, it is assummed to be located in the dame folder as the default cfgfile.
        :type addcfg: str
        :return: True if the update was successful
        :rtype: bool
        """
        if not(os.path.isfile(addcfg)):
            addcfg=os.path.normpath(os.path.join(self.myhome,addcfg)) # cfg directly in the usr/gess/ folder
            if not(os.path.isfile(addcfg)):
                print(_('{:s} does not exists').format(addcfg))
                return False
        with open(addcfg,'r') as fp:
            newoptions=DictX(permissive_json_loads(fp))
        self.updateoptions_fromdict(newoptions)
        return True

    def printoptions(self):
        """ Print out all the keys and values of the options attribute """
        for k,v in self.options.items():
            print(k+':'+str(v))

    def getoptions(self):
        """ Return the options attribute """
        return self.options

    def exportcfgfile(self,cfgfile=''):
        """Export options attribute to a cfg file, selected by the user

        :param cfgfile: optional path to save the cfgfile. If empty, will open a SaveAs window, defaults to ''
        :type cfgfile: str, optional

        :return: True if sucessfull
        :rtype: bool
        """
        if len(cfgfile)==0:
            parent=tk.Tk()
            parent.withdraw()
            cfgfile = filedialog.asksaveasfile(title=_('Save configuration file as... '),initialdir=self.myhome,initialfile=self.optiontype+'.cfg',filetypes=[('cfg file','*.cfg'),('All Files', '*.*')],defaultextension="cfg")
            parent.destroy()
        else:
            cfgfile=open(cfgfile,'w')

        if not cfgfile is None:
            json.dump(self.options,cfgfile,sort_keys=False,indent=4)
            cfgfile.close()
        return True
    
    def importcfgfile(self,update=True):
        """Import options attribute from a cfg file, selected by the user

        :param update: If True, the default cfg file is updated, defaults to True
        :type update: bool, optional
        :return: True if sucessfull
        :rtype: bool
        """
        parent=tk.Tk()
        parent.withdraw()
        cfgfile = filedialog.askopenfilename(title=_('Choose configuration file'),initialdir=self.myhome,initialfile=self.optiontype+'.cfg',filetypes=[('cfg file','*.cfg'),('All Files', '*.*')],defaultextension="cfg")
        parent.destroy()
        success=False
        if len(cfgfile)>0:
            success=self.updateoptions_fromfile(cfgfile)
            if update:
                with open(self.defcfgfile,'w') as fp:
                    json.dump(self.options,fp,sort_keys=False,indent=4)
            return success


# correct the json file if escape ('\') characters are used in the paths (windows)
def permissive_json_loads(fp):
    """loads a json file making sure the backslashes (from Windows-style paths) are correctly escaped.

    :param fp: a file object retuend by open()
    :type fp: file object
    :return: data read from the json file
    :rtype: dict
    """
    text=fp.read()
    while True:
        try:
            data = json.loads(text)
        except JSONDecodeError as exc:
            if exc.msg == 'Invalid \\escape':
                text = text[:exc.pos] + '\\' + text[exc.pos:]
            else:
                raise
        else:
            return data






if __name__ == "__main__":
    
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=')
            kwargs[f]=v
        else:
            args.append(a)
    opt=options(*tuple(args),**kwargs)


   
