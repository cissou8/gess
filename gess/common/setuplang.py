import os,sys
import PySimpleGUI as sg

def returninilanguage(update=False):
    """Returns the user language

    :param update: trigger update mode, defaults to False
    :type update: bool, optional

    :return: 'en' or 'fr'
    :rtype: str
    """    

    if os.name == 'nt':
        myhome=os.path.join(os.environ.get('USERPROFILE'),'gess')
    else:
        myhome=os.path.join(os.environ.get('HOME'),'gess')
    inifile=os.path.join(myhome,'gess.ini')

    language=''
    if os.path.isfile(inifile):
        with open(inifile,'r') as fp:
            lines = list(line for line in (l.strip() for l in fp) if (line and line.startswith('language:')))
        if len(lines)>0:
            language=lines[0].split(':',1)[1].strip()
    else: #creating empty file
        with open(inifile, 'w') as fp: 
            pass

    if len(language)==0 or update:

        sg.theme('Light Blue 3')  

        layout = [  [sg.Text('Select Language',size=(20,1))],
                    [sg.Combo(['English', 'Français'],key='lang',default_value='English')],
                    [sg.OK()] ]

        window = sg.Window('Gess set-up', layout)
 
        while True:             
            event, values = window.read()
            if event in (sg.WIN_CLOSED, 'Cancel'):
                language='en'
                break
            if event=='OK':
                if values['lang']=='Français':
                    language='fr'
                if values['lang']=='English':    
                    language='en'  
                break                              
        window.close()

    # update the ini file

    line='language:'+language
    with open(inifile,'r') as fp:
        lines=list(line for line in (l.strip() for l in fp))
    newlines=[]
    appended=False
    for l in lines:
        if l.startswith('language:'):
            newlines.append(line)
            appended=True
        else:
            newlines.append(l)
    if not appended:
        newlines.append(line)
    with open(inifile,'w') as fp:
        for l in newlines:
            fp.write(l+'\n') 
    return language


if __name__ == "__main__":

    returninilanguage(update=True)