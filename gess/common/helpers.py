import os,sys
import re
import tkinter as tk
from tkinter import filedialog
from astropy.io.fits.convenience import getheader
from astropy.io.fits import info as fitsinfo
from pysiril.siril import Siril
from pysiril.addons import Addons
from distutils.util import strtobool
import glob
import pyexiv2
from datetime import datetime,timedelta



import subprocess
import re

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess.common.i18n import _

from .DictX import DictX
from .options import options


# helper functions
def checkcalibexists(calibtype,calibfolder,calibfmt,workdir,frames,ext,app=None):
    """Checks if a calibration library of a folder containing calibration frames to stack exists

    :param calibtype: the name of calibration frames, i.e. dark,flat,bias or darkflat 
    :type calibtype: str

    :param calibfolder: either the path to the calibration frames library or the subfolder name containing calibration frames to stack
    :type calibfolder: str

    :param calibfmt: the string to parse master frame name, containing str and FITS header keys with format specifier e.g: "BIAS_O[OFFSET:d]_bin[XBINNING:d].fit"
    :type calibfmt: str

    :param workdir: the path to the working directory containing the session
    :type workdir: str

    :param frames: the string to identify the frames to be calibrated, typ. "light" or "flat"
    :type frames: str

    :param ext: the extension of the frames to be calibrated
    :type ext: str

    :param app:  a Siril class instance that can be used to call pySiril functions. Pass None if it needs to be started, defaults to None
    :type app: Siril, optional

    :return: (True if successful,True if the masters are in a library/False if they need to be stacked from the session,full path to the masterframe if found in a library)
    :rtype: (bool,bool,str)
    """
    print(_('Searching for {:s}').format(calibtype))
    if len(calibfolder)==0:
        print(_('Could not find folder containing {:s}').format(calibtype))
        return False,False,''
    calibfolder=os.path.normpath(calibfolder)
    if os.sep in calibfolder: # if '/' found in calibfolder, it means a library was specified
        hascaliblib=True
        if len(frames)>0: # if a reference frame to be calibrated is parsed
            print(_('Searching for master{0:s} matching {1:s} frames').format(calibtype,frames.lower()))
            firstframe=getfirstfile(os.path.join(workdir,frames)) # reference fame for fits header keys
            success,masterfile=findmaster(workdir,calibfolder,calibfmt,firstframe,app)
        else:
            masterfile=os.path.realpath(os.path.join(calibfolder,calibfmt))
            success=True
            if os.path.isfile(masterfile):
                success=True
        if not success:
            return False,False,''
        print(_('Master{0:s} found @ {1:s}').format(calibtype,masterfile))
    else: # if '/' not found in calibfolder, need to check that the calib subfolder exists
        hascaliblib=False
        success,_dump=checksubfolder(workdir,calibfolder)
        if not success:
            print(_('Could not find folder containing {:s}').format(calibtype))
            return False,False,''
        print(_('{:s} folder found').format(calibtype))
        masterfile=''
    return True,hascaliblib,masterfile


def checklocalcalibexists(calibtype,calibfolder,calibfmt,frames,ext,app=None):
    """Checks if a local copy of the master exists in the working directory (in the masters subfolder)

    :param calibtype: the name of calibration frames, i.e. dark,flat,bias or darkflat 
    :type calibtype: str

    :param calibfolder: the path to the local calibration frames library
    :type calibfolder: str

    :param calibfmt: the string to parse master frame name, containing str and FITS header keys with format specifier e.g: "BIAS_O[OFFSET:d]_bin[XBINNING:d].fit"
    :type calibfmt: str


    :param frames: the string to identify the frames to be calibrated, typ. "light" or "flat"
    :type frames: str

    :param ext: the extension of the frames to be calibrated
    :type ext: str

    :param app:  a Siril class instance that can be used to call pySiril functions. Pass None if it needs to be started, defaults to None
    :type app: Siril, optional

    :return: (True if master exists, full path to the masterframe if found)
    :rtype: (bool,str)
    """
    print(_('Searching for {:s} in local library').format(calibtype))
    calibfolder=os.path.normpath(calibfolder)
    workdir=os.path.normpath(os.path.join(calibfolder,'..'))
    normnames={ 'dark':'dark_stacked',
                'flat':'pp_flat_stacked',
                'bias':'offset_stacked',
                'darkflat':'offset_stacked',
                }

    normmastername='{0:s}.{1:s}'.format(normnames[calibtype],ext)
    normmastername=os.path.join(calibfolder,normmastername)
    if os.path.isfile(normmastername):
        print(_('Master{0:s} found @ {1:s}').format(calibtype,normmastername))
        return True,normmastername
    if os.path.isdir(os.path.join(workdir,frames)):
        firstframe=getfirstfile(os.path.join(workdir,frames)) # reference fame for fits header keys
        success,masterfile=findmaster(workdir,calibfolder,calibfmt,firstframe,app)
        if not success:
            return False,''
        print(_('Master{0:s} found @ {1:s}').format(calibtype,masterfile))
        return True,masterfile
    else:
        print(_('None found'))
        return False,''



def getfirstfile(folder):
    """Finds the first file of a folder with given extension

    :param folder: the path to search
    :type folder: str


    :return: the path to the first file of the folder if one was found, an empty string otherwise
    :rtype: str
    """
    validexts=GetSirilRawExt()
    validexts+=['.fit','.fits','.fts']
    if len(folder)==0:
        folder=os.getcwd()
    with os.scandir(folder) as it:
        for entry in it:
            if not entry.name.startswith('.'):
                if os.path.splitext(entry.name.lower())[1] in validexts:
                    return entry.path
    return ''

def parsemasterformat(masterfmt,hdr,refframe,mode='r',rmspace=True):
    """Parses the name of a master based on its formatting string and the header of the frame to calibrate

    :param masterfmt: a string containing the format specification to parse the name. 
    :type masterfmt: str

    All the string tokens between brackets will be parsed. The tokens are formed with:
    - HEADERKEY: a valid key of the FITS header
    - fmt: a format specifier. Most of the time d, f or s should do.

    You can have a look at https://docs.python.org/3/library/string.html#formatstrings

    Special wildcard \"\*\" character before HEADERKEY:
    If a HEADERKEY is suffixed with a wildcard character, the string returned will change,
    depending on **mode** value:

    - If mode='r': [\*HEADERKEY:fmt] is replaced by \*

    - If mode='w': HEADERKEY is parsed 
 
    :param hdr: A dictionnary of **refframe** header keys, as returned by pySiril.Addons::ReadFITSHeader
    :type hdr: dict
 
    :param refframe: the name of the reference frame
    :type refframe: str

    :param mode: either 'r' or 'w'. Flag to handle behavior with wildcards, defaults to 'r'
    :type mode: str, optional

    :param mode: Flag to remove spaces in the output string, defaults to True
    :type mode: str, optional

    :return: the name of the file with header keys parsed as per specification.
    :rtype: str
    """  
    l = re.findall(r'\[(.*?)\]', masterfmt) 
    masterfile=masterfmt
    vals=[]
    p=0
    for s in l:
        place='{:1d}'.format(p)
        ss=s[:]
        if s.startswith('*'):
            if mode=='r':
                masterfile=masterfile.replace('['+s+']','*')
                continue
            elif mode=='w':
                ss=s[1:]
            else:
                print(_('Unknown mode for parsemasterformat'))
                return ''
        field,fmt=ss.split(':')
        try:
            _dump=hdr[field]
        except:
            print(_('Could not find {0:s} key in the FITS header of {1:s}, please check and retry').format(field,refframe))
            return ''
        if 'dm12' in fmt:
            try:
                dt=datetime.strptime(hdr[field],'%Y-%m-%dT%H:%M:%S.%f')
                dtm12=dt-timedelta(hours=12)
                vals.append(dtm12.strftime('%Y-%m-%d'))
                fmt='s'
            except:
                print(_('Field {0:s} with value {1:s} could not be converted to date, please check and retry').format(field,str(field)))
                return ''
        elif 'dm0' in fmt:
            try:
                dt=str(hdr[field])
                vals.append(dt.split('T')[0])
                fmt='s'
            except:
                print(_('Field {0:s} with value {1:s} could not be converted to date, please check and retry').format(field,str(field)))
                return ''
        elif 'ra' in fmt:
            try:
                ra=str(hdr[field])
                ra=ra.replace(' ','h',1)
                ra=ra.replace(' ','m',1)
                ra+='s'
                vals.append(ra)
                fmt='s'
            except:
                print(_('Field {0:s} with value {1:s} could not be converted to right ascension, please check and retry').format(field,str(field)))
                return ''
        elif 'dec' in fmt:
            try:
                dec=str(hdr[field])
                dec=dec.replace(' ','d',1)
                dec=dec.replace(' ','m',1)
                dec+='s'
                vals.append(dec)
                fmt='s'
            except:
                print(_('Field {0:s} with value {1:s} could not be converted to declination, please check and retry').format(field,str(field)))
                return ''
        elif 'd' in fmt: #Converting to int if the format requires it
            vals.append(int(float(hdr[field]))) # to avoid silly int() error
        elif 'f' in fmt:
            vals.append(float(hdr[field])) 
        else:
            tmp=hdr[field].replace('/','_')
            vals.append(tmp)
        masterfile=masterfile.replace('['+s+']','{'+place+':'+fmt+'}')
        p+=1
    masterfile=masterfile.format(*vals)
    if rmspace:
        masterfile=masterfile.replace(' ','')
    return masterfile

def findmaster(workdir,master,masterfmt,refframe,app=None):
    """Looks for a suitable master for a given reference frame

    :param workdir: the path to the working directory containing the session
    :type workdir: str

    :param master: either the full path to the masters library or subfolder string wrt. workdir
    :type master: str

    :param masterfmt: check-out spec in parsemasterformat
    :type masterfmt: str
    
    :param refframe: the name of the reference frame
    :type refframe: str

    :param app:  a Siril class instance that can be used to call pySiril functions. Pass None if it needs to be started, defaults to None
    :type app: Siril, optional

    :return:  (True if the master was found,the full path to the master if one was found)
    :rtype: (bool,str)
    """
    if not(os.path.isabs(master)): # in case path is relative
        masterlibrary=os.path.realpath(os.path.join(workdir,master))
    else:
        masterlibrary=os.path.realpath(master)
    if not(os.path.isdir(masterlibrary)):
        print(_('The specified master library path {:s} does not exist, please check and retry').format(masterlibrary))
        return False,''

    # reader header of the reference frame
    needtoclose=False
    if app is None:
        app=Siril(requires='0.99.8.1')
        needtoclose=True

    validexts=GetSirilRawExt()
    validexts+=['.fit','.fits','.fts']
    ext= os.path.splitext(refframe)[1]
    if not(ext.lower() in validexts):
        print(_('File {0:s} does not have a valid extension, please check and retry').format(refframe))
        return False,''
    hdr=GetHeader(refframe)
    if len(hdr)==0:
        print(_('File {0:s} does not have a valid header, please check and retry').format(refframe))
        return False,''       

    if needtoclose:
        del app
    _dump,refframe=os.path.split(refframe)

    masterfile=parsemasterformat(masterfmt,hdr,refframe)
    if len(masterfile)==0:
        print(_('None found'))
        return False,''
    files=glob.glob(os.path.join(masterlibrary,masterfile))
    if len(files)==0:
        print(_('Could not find suitable master: {:s} in your master library').format(masterfile))
        return False,''
    if len(files)>1:
        print(_('Found more than one file matching pattern :{0:s}').format(masterfile))
        print(_('Reconsider your naming convention - Using the first found'))

    
    return True,files[0]

def pathhasspace(folder):
    """Returns True if the path has spaces

    :param folder: a path
    :type folder: str

    :return: True if the path has spaces
    :rtype: bool
    """
    if ' ' in folder:
        print(_('{:s} has spaces and cannot be used to build scripts')).format(folder)
        return True
    return False

def checksubfolder(workdir,subfolder):
    """Checks if a subfolder exists

    :param workdir: the path to the root folder to be searched
    :type workdir: str

    :param subfolder: the subfolder name to be found
    :type subfolder: str

    :return:   (True if the subfolder exists,The full path to the subfolder)
    :rtype: (bool,str)
    """   
    if len(subfolder)==0:
        return False,''
    subpath=os.path.realpath(os.path.join(workdir,subfolder))
    if not(os.path.isdir(subpath)):
        return False,''
    return True,subpath

def relativizepath(refframe,workdir):
    """Returns the path of a file relative to a given directory

    :param refframe: the path to a file
    :type refframe: str
    :param workdir:  the path to a folder
    :type workdir: str
    :return: the path of refframe relative to workdir, with all separators replaced with \"/\"
    :rtype: str
    """  
    refframe=os.path.relpath(refframe,workdir)
    return refframe.replace(os.sep,'/')


def fast_scandir(dirname):
    """Returns all the subfolders of a given path

    :param dirname: the path to a folder
    :type dirname: str

    :return: the list of all subpaths of dirname (full paths)
    :rtype: list(str)
    """
    subfolders= [f.path for f in os.scandir(dirname) if f.is_dir()]
    for dirname in list(subfolders):
        subfolders.extend(fast_scandir(dirname))
    return subfolders 

def checkmastersubs(mastertype,masters,opt,subbydate,app=None,findsubsets=True,follownaming=''):
    """Returns all the subsets of masters in a path

    :param mastertype: a name to be used in print outs, giving the type of files being searched
    :type mastertype: str

    :param masters: the last part of a path corresponding to these masters
    :type masters: str

    :param opt: options member of an Options instance, with optiontype='gess'
    :type opt: DictX

    :param subbydate: a set of folders to search
    :type subbydate: list(str)

    :param app: a Siril class instance that can be used to call pySiril functions.
                Pass None if it needs to be started, defaults to None
    :type app: Siril, optional

    :param findsubsets: True if the headers of the files need to be verified to identify 
            unique sets of frames, defaults to True
    :type findsubsets: bool, optional

    :param follownaming: the naming convention to be used to name the subsets, defaults to ''
    :type follownaming: str, optional

    :return: the list of all the subsets paths
    :rtype: list(str)
    """ 
    mastersubs=[]
    print(_('Searching for {:s} subfolders').format(mastertype))
    for d in subbydate:
        allsubs=fast_scandir(d)
        for a in allsubs:
            if (masters.lower()==os.path.split(a)[1].lower() ) and getfirstfile(a):
                print(a)
                mastersubs.append(a)
    if len(mastersubs)==0:
        print(_('No {:s} folder found matching the date').format(mastertype))
    print('')
    mastersubs.sort(key=os.path.getctime)

    if findsubsets:
        #identifying if multiple sets have been stored in the same folder
        needtoclose=False
        if app is None:
            app=Siril(requires='0.99.8.1')
            needtoclose=True
        app.tr.Configure(False,False,True,True)
        AO=Addons(app)  
        mastersubsets=[]
        for _n,a in enumerate(mastersubs):
            frames=glob.glob(os.path.join(a,'*.'+opt.ext))
            hdrs={}
            i=0
            for f in frames:
                temp={}
                temp['filename']=f
                hdr=GetHeader(f)
                if len(follownaming)==0:
                    ID=parsemasterformat(opt[mastertype+'fmt'],hdr,f)
                else:
                    ID=parsemasterformat(follownaming,hdr,f)
                temp['ID']=ID
                hdrs[i]=temp
                i+=1
            IDs=[hdrs[i]['ID'] for i in range(len(hdrs))]
            sets=list(set(IDs))
            if (len(sets)>1)|(len(follownaming)>0): #need to split them in different folders before calling convert. Dispatch lights in any case
                oneup,_dump=os.path.split(a)
                for i,s in enumerate(sets):
                    setfiles=[hdrs[j]['filename'] for j in range(len(hdrs)) if hdrs[j]['ID']==s]
                    if len(follownaming)==0:
                        copydir=os.path.join(oneup,'{0:s}{1:02d}'.format(masters,i))
                    else:
                        hdr=GetHeader(setfiles[0])
                        copydir=parsemasterformat(follownaming,hdr,setfiles[0])
                        copydir=os.path.join(oneup,copydir)
                    AO.MkDirs(copydir)
                    print(_('Creating and filling subset folder {0:s}').format(copydir))
                    for f in setfiles:
                        AO.CopyLink(f, os.path.join(copydir,os.path.split(f)[1]), bCopyMode=False)
                    print('')
                    mastersubsets.append(copydir)
            else:
                mastersubsets.append(a)
        if needtoclose:
            del app

    else:
        mastersubsets=mastersubs
    return mastersubsets

def checkmastersconfiguration(mastertype,opt,loop):
    """Checks the masters configuration as per loop spec

    :param mastertype: the types of masters being checked
    :type mastertype: str

    :param opt: options member of an Options instance, with optiontype='gess'
    :type opt: DictX

    :param loop: options member of an Options instance, with optiontype='loop'
    :type loop: DictX

    :return: (True if the check succeeded,True if the masters need to be stacked and copied to a library,the final pathbit of the masters,the format to parse the masters names)
    :rtype: (bool,bool,str,str,str)
    """
    copy2lib=False
    if len(loop[mastertype+'2lib'])>0:
        copy2lib=True
        masters=os.path.normpath(loop[mastertype+'2lib'])
        if os.sep in masters:
            print(_('loop.cfg needs to specify a simple string, not a path, where the {:s} are stored').format(mastertype))
            return False,True,'','',''                
        masterlib=os.path.normpath(opt[mastertype])
        if not os.path.isdir(masterlib): #creating the lib folder in case it does not exist
            os.makedirs(masterlib,exist_ok=True)
        if not os.sep in masterlib:
            print(_('gess.cfg needs to specify the path to the {:s} library').format(mastertype))
            return False,True,'','',''
        mastersfmt=opt[mastertype+'fmt']
        if len(mastersfmt)==0:
            print(_('gess.cfg needs to specify the format to name the {:s} masters').format(mastertype))
            return False,True,'','',''
        return True,copy2lib,masters,masterlib,mastersfmt
    else:
        masters=opt[mastertype]
        return True,False,masters,'',''

def masterstackingoptions(opt,mastertype,library=False):
    """Returns gess options with only one type of masters to be stacked

    :param opt: options member of an Options instance, with optiontype='gess'
    :type opt: DictX

    :param mastertype: the type of masters to be activated
    :type mastertype: str

    :param library: True if the masters will be copied to a library. Activates 16b with no compression. Defaults to False
    :type library: bool, optional

    :return:  options member of an Options instance, with optiontype='gess', with preprocessing and compression options set to only stack one type of masters
    :rtype: DictX
    """
    optout=DictX(opt)
    for k in optout.keys():
        if k.startswith('pp') and not(mastertype) in k:
            optout[k]=False
        if k.startswith('do'):
            optout[k]=False
    if library:
        optout.compression='0'
        optout.bitdepth=16   
    if mastertype=='flat':
        optout.ppoffset=True
    return optout 


def returninifolder(inifile,boxtitle=None):
    """Returns the last session working directory

    :param inifile: the path to gess.ini file
    :type inifile: str

    :param boxtitle: The message to be displayed at the top of the folder chooser, defaults to None
    :type boxtitle: str, optional

    :return: the path to the selected session folder. gess.ini is updated with the new path
    :rtype: str
    """

    workdir=''
    if os.path.isfile(inifile):
        with open(inifile,'r') as fp:
            lines = list(line for line in (l.strip() for l in fp) if (line and line.startswith('start:')))

        if len(lines)>0:
            workdir=os.path.realpath(lines[0].split(':',1)[1].strip())
    else: #creating empty file
        with open(inifile, 'w') as fp: 
            pass
        
    if len(workdir)==0:
        workdir=os.getcwd()

    parent=tk.Tk()
    parent.withdraw()
    if boxtitle is None:
        boxtitle=_('Please select the session working directory')

    workdir = filedialog.askdirectory(initialdir=workdir,title=boxtitle)
    parent.destroy()


    # update the ini file
    if len(workdir)>0: # in case Cancel was pressed
        workdir=os.path.realpath(workdir)
        line='start:'+workdir
        with open(inifile,'r') as fp:
            lines=list(line for line in (l.strip() for l in fp))
        newlines=[]
        appended=False
        for l in lines:
            if l.startswith('start:'):
                newlines.append(line)
                appended=True
            else:
                newlines.append(l)
        if not appended:
            newlines.append(line)
        with open(inifile,'w') as fp:
            for l in newlines:
                fp.write(l+'\n') 
    return workdir



def ReadSirilPrefs(app=None):
    """Reads Siril Preferences file

    :param app: a Siril class instance that can be used to call pySiril functions. Pass None if it needs to be started, defaults to None, defaults to None
    :type app: Siril, optional

    :return: a dictionnary with Siril configuration
    :rtype: dict
    """

    needtoclose=False
    if app is None:
        app=Siril(requires='0.99.8.1')
        needtoclose=True
    AO=Addons(app)  

    prefs=AO.GetSirilPrefs()
    if needtoclose:
        del app
    return prefs


def ParseSirilBDC(prefs=None,app=None):
    """Parses Siril bitdepth and compression settings

    :param prefs: a dictionnary containong Siril preferences. If None, pySiril Addons::GetSirilPrefs() is called.
    :type prefs: dict, optional

    :param app: a Siril class instance that can be used to call pySiril functions. Pass None if it needs to be started, defaults to None, defaults to None
    :type app: Siril, optional

    :return: a DictX with compression and bitdepth commands
    :rtype: DictX
    """
    needtoclose=False
    if app is None:
        app=Siril(requires='0.99.8.1')
        needtoclose=True
    AO=Addons(app)  
    if prefs is None:
        prefs=AO.GetSirilPrefs()
    
    compset=prefs['compression-settings']

    if 'fits_enabled' in compset: #siril 0.99.10 - new keyword in prefs
        compkw='fits_'
    else:
        compkw='compress_'
    if not strtobool(compset[compkw+'enabled']):
        compression= '0'
    else:
        compmethods={
            '0':'rice',
            '1':'gzip1',
            '2':'gzip2',
            '3':'hcompress'
        }

        compression='1'
        compression+=' -type='+compmethods[compset[compkw+'method']]
        compression+=' {:d}'.format(int(float(compset[compkw+'quantization'])))
        if 'hcompress' in compression:
            compression+=' {:d}'.format(int(float(compset[compkw+'hcompress_scale'])))  

    miscprefs=prefs['misc-settings']
    if miscprefs['FITS_type']=='0':
        bitdepth=16
    if miscprefs['FITS_type']=='1':
        bitdepth=32


    if needtoclose:
        del app
    command=DictX()
    command.compression=compression
    command.bitdepth=bitdepth
    return command


def GetSirilBitDepth(prefs=None,app=None):
    """Returns Siril bitdepth preferences

    :param prefs: a dictionnary containong Siril preferences. If None, pySiril Addons::GetSirilPrefs() is called.
    :type prefs: dict, optional

    :param app: a Siril class instance that can be used to call pySiril functions. Pass None if it needs to be started, defaults to None
    :type app: Siril, optional
    
    :return: \"16\" or \"32\" 
    :rtype: str
    """
    needtoclose=False
    if app is None:
        app=Siril(requires='0.99.8.1')
        needtoclose=True
    AO=Addons(app)  
    if prefs is None:
        prefs=AO.GetSirilPrefs()
    
    miscset=prefs['misc-settings']
    bd=''

    if miscset['FITS_type']=='0':
        bd='16'

    if miscset['FITS_type']=='1':
        bd='32'


    if needtoclose:
        del app
    return bd

def GetSirilRawExt():
    """Returns Siril list of RAW extensions

    :return: a list of valid extensions for raw format as returned by siril -f
    :rtype: list(str)

    """
    #TODO:need to find smthg smarter
    # if len(sirilexe)==0:
    #     a=subprocess.run(['siril', '-f'],stdout=subprocess.PIPE)
    # else:
    #     a=subprocess.run([sirilexe, '-f'],stdout=subprocess.PIPE)
    # lines=a.stdout.decode('utf-8')
    # lines=lines.split('\n')
    # exts=''
    # for l in lines:
    #     if l.lower().startswith('raw'):
    #         exts=re.findall(r'\*(.*?),',l)
    exts=['.dng', '.mos', '.cr2', '.crw', '.cr3', '.bay', '.erf', '.raf', '.3fr', '.kdc', '.dcr', '.mef', '.mrw', '.nef', '.nrw', '.orf', '.raw', '.rw2', '.pef', '.ptx', '.x3f', '.srw']

    return exts

def ReadRawHeader(filename,validrawexts=None):
    """Read raw exif and returns a dictionnary with the following keys:

    - INSTRUMEN: the name of the camera
    - ISOSPEED: the iso setting of the shot
    - EXPTIME: the exposure time in s
    - DATE-LOC: the date and time of the exposure

    :param filename: full path to the raw file to read headers from.
    :type filename: str

    :param validrawexts: a list of valid raw files extensions, defaults to None
    :type validrawexts: list(str)

    :return: a dictionnary with keys listed above
    :rtype: dict

    """
    ext=os.path.splitext(filename)[1].lower()
    if validrawexts is None:
        validrawexts=GetSirilRawExt()
    if not(ext in validrawexts):
        print(_('RAW file {:s} does not have a valid raw extension as per Siril recognized RAW formats - type siril -f in a shell to learn more').format(filename))
        return {}

    hdr={}
    pyexiv2.set_log_level(4)
    try:
        with pyexiv2.Image(filename) as img:
            data = img.read_exif()
    except:
        print(_('RAW file {:s} exif data could not be extracted').format(filename))
        return {}



    hdr['ISOSPEED']=int(data['Exif.Photo.ISOSpeedRatings'])
    hdr['EXPTIME']=round(float(eval(data['Exif.Photo.ExposureTime'])),2)
    hdr['INSTRUME']=data['Exif.Image.Model']
    dt=datetime.strptime(data['Exif.Image.DateTime'],'%Y:%m:%d %H:%M:%S')
    hdr['DATE-OBS']=dt.strftime('%Y-%m-%dT%H:%M:%S.000')

    return hdr

def GetHeader(refframe):

    validrawexts=GetSirilRawExt()
    validfitsexts=['.fit','.fits','.fts']
    ext= os.path.splitext(refframe)[1]
    if ext.lower() in validrawexts:
        hdr=ReadRawHeader(refframe,validrawexts)
    elif ext.lower() in validfitsexts:
        temp=fitsinfo(refframe,output=False)
        # with fits.open(refframe,mode='readonly') as hdulo:
        if len(temp)==1: #regular fit
            hdr=getheader(refframe)
        elif len(temp[0][5])==0: #compressed fit - concatenate primary and first extension
            hdr=getheader(refframe)
            hdr=dict(hdr,**getheader(refframe,1))
        else: #fitseq - read first frame header
            hdr=getheader(refframe)
    
    return hdr
    

