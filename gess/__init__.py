import os
from .common.options import options
Options=options(optiontype='gess',updatedefault=True)
options(optiontype='loop',updatedefault=True)
options(optiontype='copyback',updatedefault=True)

#fix for error language
inifile=os.path.join(Options.myhome,'gess.ini')
if not os.path.isfile(inifile):
    with open(inifile, 'w') as fp: 
        pass

