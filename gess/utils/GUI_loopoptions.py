import PySimpleGUI as sg
import os,sys

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess.common.options import options
from gess.common.DictX import DictX
from gess.common.i18n import _



def Run():

    sg.theme('Light Blue 3')

    msgdict={
        'imagingfolder': 'Imaging Folder: ',
        'datefmt': 'Date format:',
        'darks2lib': 'Darks for library:',
        'flats2lib': 'Flats for library:',
        'biases2lib': 'Biases for library:',  
        'darkflats2lib': 'Darkflats for library:',    
        'NINAflatwizard': 'NINA flats naming convention:',
        'lightsfolders': 'Lights sets naming convention:',

    }

    tooltipsdict={
        'imagingfolder': _('Define the path where all your sessions are stored.\n\nloop.cfg syntax:imagingfolder=\"\"'),
        'datefmt': _('Give the string to parse the date format\nExample \""%Y-%m-%d\"\n\nloop.cfg syntax:datefmt=\""%Y-%m-%d\"'),
        'darks2lib': _('Give the name of the folder that will contain the dark frames to stack\nOnce stacked, all the masters will be stored in your darks library\n\nloop.cfg syntax:darks2lib=\"\"\nNote: if left blank, gessloop will not look for darks to fill your library'), 
        'flats2lib': _('Give the name of the folder that will contain the flat frames to stack\nOnce stacked, all the masters will be stored in your flats library\n\nloop.cfg syntax:flats2lib=\"\"\nNote: if left blank, gessloop will not look for flats to fill your library'), 
        'biases2lib': _('Give the name of the folder that will contain the bias frames to stack\nOnce stacked, all the masters will be stored in your biases library\n\nloop.cfg syntax:biases2lib=\"\"\nNote: if left blank, gessloop will not look for biases to fill your library'), 
        'darkflats2lib': _('Give the name of the folder that will contain the darkflat frames to stack\nOnce stacked, all the masters will be stored in your darkflats library\n\nloop.cfg syntax:darkflats2lib=\"\"\nNote: if left blank, gessloop will not look for darkflats to fill your library'), 
        'NINAflatwizard': _('Special key for NINA users. When filled, it will tell gessloop that flats can be stored elsewhere than together with the lights and how to assign them correctly to each light.\nExample: \"FLAT_[FILTER:s]_O[OFFSET:d]_bin[XBINNING:d]\" without extension.\n\nloop.cfg syntax:NINAflatwizard=\"\"\nNote: It is somewhat similar to using a flats library except the masterflats won\'t be persistent in between your sessions'), 
        'lightsfolders': _('If multiple sets of lights can be stored in the same light folder, give a convention to rename every set\nExample: \"LIGHT_F[FILTER:s]_[EXPTIME:d]s_G[GAIN:d]_bin[XBINNING:d]" without extension.\n\nloop.cfg syntax:lightsfolders=\"\"'),     
    }

    warndict={
        'Clear': _('You are about to clear all data in this form.\nDo you want to proceed?'),
        'Update Default': _('You are about to save all these values as the new default options\nDo you want to proceed?'),
    }


    def _libinput(name,opt,msgdict,tooltipsdict):
        return [[   sg.Text(msgdict[name],size=(14,1)),
                    sg.Input(opt[name],key=name,tooltip=tooltipsdict[name],size=(25,1))]]

    def _folderinput(name,opt,msgdict,tooltipsdict):

        return [[   sg.Text(msgdict[name],size=(22,1)),
                    sg.Input(opt[name],key=name,tooltip=tooltipsdict[name],size=(70,1))]]


    def updateoptions(wd,opt):
        updict={}
        for k in opt.keys():
            updict[k]=wd[k].Get()
        return updict

    def warningwd(warntext):
        return sg.popup_ok_cancel(_('Warning')+': '+warntext,title=_('Warning'))



    Opt=options('loop')
    opt=Opt.getoptions()


    loop_layout = []
    loop_layout+=[[sg.Text('GeSS Loop options', font='Any 12')]]
    loop_layout+=[[sg.Text(msgdict['imagingfolder'],size=(14,1)),sg.Input(opt['imagingfolder'],key='imagingfolder',tooltip=tooltipsdict['imagingfolder'],size=(66,1)),sg.FolderBrowse(target='imagingfolder',key='imagingfolderbrowse',tooltip=tooltipsdict['imagingfolder'])]]
    loop_layout+=_libinput('datefmt',opt,msgdict,tooltipsdict)
    loop_layout+=_libinput('darks2lib',opt,msgdict,tooltipsdict)
    loop_layout+=_libinput('flats2lib',opt,msgdict,tooltipsdict)
    loop_layout+=_libinput('biases2lib',opt,msgdict,tooltipsdict)
    loop_layout+=_libinput('darkflats2lib',opt,msgdict,tooltipsdict)
    # loop_layout+=_folderinput('NINAflatwizard',opt,msgdict,tooltipsdict)
    # loop_layout+=_folderinput('lightsfolders',opt,msgdict,tooltipsdict)

    buttonsize=11

    loop_layout+=[[          
                sg.Button(_('Clear'),tooltip=_('Reset current options to default values'),size=(buttonsize,1)),
                    sg.Button(_('Load'),tooltip=_('Load an existing cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Update Default'),tooltip=_('Update loop default cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Save As'),tooltip=_('Save as an optional cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Print'),tooltip=_('Print options'),size=(buttonsize,1)),
                ]]
    window = sg.Window(_('GeSS loop options wizard'), loop_layout, default_element_size=(50,2),finalize=True)    



    while True: 

        event, values = window.read()  
        if event == sg.WIN_CLOSED:   
            break        # always,  always give a way out!    
               

        if event==_('Clear'):
            popup=warningwd(warndict['Clear'])
            if popup.lower()=='ok':
                Opt=options(optiontype='loop',returnclean=True)
                opt=Opt.getoptions()
                for k in opt.keys():
                    window[k].Update(value=opt[k])


        if event==_('Load'):
            options.importcfgfile(Opt,update=False)
            opt=Opt.getoptions()
            for k in opt.keys():
                window[k].Update(value=opt[k])

        if event==_('Update Default'):
            popup=warningwd(warndict['Update Default'])
            if popup.lower()=='ok':
                Opt=options(optiontype='loop',returnclean=True)
                opt=Opt.getoptions()
                newvals=updateoptions(window,opt)
                Opt=options(optiontype='loop',dictcfg=newvals,updatedefault=True)

        if event==_('Save As'):
            Opt=options(optiontype='loop',returnclean=True)
            opt=Opt.getoptions()
            newvals=updateoptions(window,opt)
            Opt=options(optiontype='loop',dictcfg=newvals,updatedefault=False)
            Opt.exportcfgfile()
           

        if event==_('Print'):
            for k in opt.keys():
                if k in values.keys():
                    print(k+':'+str(values[k]))


            
    return True

if __name__ == "__main__":

    Run()