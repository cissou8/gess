import PySimpleGUI as sg
import os,sys

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess.common.options import options
from gess.common.DictX import DictX
from gess import gessi
from gess.common.i18n import _

#TODO:link to ppdark.ppflat etc... 


def Run(action='set'):

    sg.theme('Light Blue 3')

    msgdict={
        'ppdark': _('Preprocess lights with darks'),
        'ppflat': _('Preprocess lights with flats'),
        'ppoffset': _('Preprocess flats with offsets (bias or darkflats)'),
        'pplight': _('Preprocess lights'),
        'dobkg': _('Extract background before registration'),
        'doHO': _('Separate Ha and OIII layers'),
        'doregister': _('Perform registration'),
        'dostack': _('Perform stacking'),
        'debug': _('Save logs (Strongly recommended)'),
        'sirilexe': _('Siril location'),
        'seqasfitseq': _('Assemble fits as a fitseq file'),
        'compression': _('Compression'),
        'lights': _('Name of lights folder'),
        'lightsfmt': _('Rename lights'),
        'darkslib': _('I\'m using a darks library'),
        'darks': _('Name of darks folder'),
        'darksfmt': _('Darks format'),
        'flatslib': _('I\'m using a flats library'),
        'flats': _('Name of flats folder'),
        'flatsfmt': _('Flats format'),
        'biaseslib': _('I\'m using a biases library'),
        'biases': _('Name of biases folder'),
        'biasesfmt': _('Biases format'),
        'darkflatslib': _('I\'m using a darkflats library'),
        'darkflats': _('Name of darkflats folder'),
        'darkflatsfmt': _('Darkflats format'),
        'copymasters':_('Copy masters'),

    }

    tooltipsdict={
        'ppdark': _('If checked, masterdark will be susbtracted from lights.\nIf you\'re not using a darks library, dark frames will be stacked before lights preprocessing\n\ngess.cfg syntax:ppdark=true/[false]'),
        'ppflat': _('If checked, lights will be divided by masterflat.\nIf you\'re not using a flats library, flat frames will be stacked before lights preprocessing\n\ngess.cfg syntax:ppflat=true/[false]\nNote: if ppflat=true, then ppoffset=true in gess.cfg'), 
        'ppoffset': _('When checked, masteroffset will be substracted from flats before stacking.\nIf you\'re not using an offsets library, offset frames will be stacked before flats preprocessing\n\ngess.cfg syntax:ppoffset=true/[false]\nNote: ppoffset value is determmined by ppflat value, this checkbox value cannot be changed'), 
        'pplight': _('When checked, lights will be preprocessed as per options above and debayered if necessary.\ngess.cfg syntax:pplight=true/[false]\nNote: pplight value must be equal to true in most cases, this checkbox value cannot be changed'), 
        'dobkg': _('If checked, gess will call Siril background extraction with order 1\n\ngess.cfg syntax:dobkg=true/[false]'),
        'doHO': _('If checked, gess will call Siril extract Ha/OIII\n\ngess.cfg syntax:doHO=false/[true]'),
        'doregister': _('If checked, gess will call Siril registration\n\ngess.cfg syntax:doregister=true/[false]'), 
        'dostack': _('If checked, gess will call Siril stacking with following parameters:\n-norm: additive with scaling\n-rej: average with rejection (windsorized sigma-clipping)\n- sigma high and low =3\n\ngess.cfg syntax:dostack=true/[false]'),       
        'debug': _('If checked, gess and its modules will keep logs of their actions\nIt is strongly suggested to keep this option ticked.\nOtherwise, it will not be possible to get any assistance.\n\ngess.cfg syntax:debug=true/[false]'),
        'sirilexe': _('For advanced users only! This option can be used to point to a dev version\nNormal case: Leave blank to use default Siril location\n\ngess.cfg syntax:sirilexe=somepath'),
        'ext': _('Extension of image files as saved by your imaging software\nIf raw, gess will call Siril convertraw command instead of convert - to be used only if you save other file types(e.g *.jpg) together with your DSLR raw files\n\ngess.cfg syntax:ext=fits/[fit/fts/raw]'),
        'shottype': _('Choose cfa (color) or mono\n\ngess.cfg syntax:shottype=cfa/[mono]'),
        'seqasfitseq': _('If checked, individual images will be gathered into a fitseq file.\nAnd you can surely thank @vinvin who is responsible for this feature which will help you\nkeep your folders clean and tidy instead of having zillions of files all over the place\n\ngess.cfg syntax:seqasfitseq=false/[true]'),
        'bitdepth': _('Bitdepth of the processed images\n\ngess.cfg syntax:bitdepth=16/[32]'),
        'compression': _('Compression string as defined in:\nhttps://free-astro.org/index.php/Siril:Commands#setcompress \nYou should omit the setcompress string e.g. simply enter 0 for no compression\nLeave blank if you want to apply Siril settings\n\ngess.cfg syntax:compression=[0/1 -type=rice 16]'),
        'lights': _('Give the name of the folder that will contain the light frames\nCould be LIGHT (NINA), lights (APT) etc... \nNot case sensitive on Win, Case sensitive on Linux/Mac\n\ngess.cfg syntax:lights=*str*'),
        'lightsfmt': _('Optional: give the basename of the converted light sequence (no extension!). It can contain values from header to be parsed\ne.g. LIGHT_[OBJECT:s]_[TELESCOP:s]_[FILTER:s]_[EXPTIME:d]s\nThis name will then be passed on to all the sequences down to the resulting stack\n\ngess.cfg syntax:lightsfmt=*str*'),
        'darkslib': _('If checked, you will need to specify the path to the masterdarks library and the name convention of your masterdarks'),
        'darks': _('Give the name of the folder that will contain the dark frames\nCould be DARK (NINA), darks (APT) etc... \nNot case sensitive on Win, Case sensitive on Linux/Mac\nIf you have checked \"I\'m using a library\", browse to the folder where you store yor masterdarks\n\ngess.cfg syntax:darks=*str*'),
        'darksfmt': _('Give the name pattern for finding the masterdarks\nMasterdark name is formed using values from the lights FITS header\ne.g. DARK_[EXPTIME:d]s_G[GAIN:d]_O[OFFSET:d]_T[SET-TEMP:d]C_bin[XBINNING:d].fits\nIn the example above, value from key EXPTIME will be converted to an interger (as specified with the :d)\n\ngess.cfg syntax:darksfmt=*str*'),
        'flatslib': _('If checked, you will need to specify the path to the masterflats library and the name convention of your masterflats'),
        'flats': _('Give the name of the folder that will contain the flat frames\nCould be FLAT (NINA), flats (APT) etc... \nNot case sensitive on Win, Case sensitive on Linux/Mac\nIf you have checked \"I\'m using a library\", browse to the folder where you store yor masterflats\n\ngess.cfg syntax:flats=*str*'),
        'flatsfmt': _('Give the name pattern for finding the masterflats\nMasterflat name is formed using values from the lights FITS header\ne.g. FLAT_[TELESCOP:s]_[FILTER:s]_bin[XBINNING:d].fits\nIn the example above, value from key FILTER will be converted to a string (as specified with the :s)\n\ngess.cfg syntax:flatsfmt=*str*'),
        'biaseslib': _('If checked, you will need to specify the path to the masterbiases library and the name convention of your masterbiases'),
        'biases': _('Give the name of the folder that will contain the biases frames\nCould be BIAS (NINA), biases (APT) etc... \nNot case sensitive on Win, Case sensitive on Linux/Mac\nIf you have checked \"I\'m using a library\", browse to the folder where you store yor masterbiases\n\ngess.cfg syntax:biases=*str*'),
        'biasesfmt': _('Give the name pattern for finding the masterbiases\nMasterbias name is formed using values from the flats FITS header\ne.g. "BIAS_O[OFFSET:d]_bin[XBINNING:d].fit"\nIn the example above, value from key OFFSET will be converted to an integer (as specified with the :d)\n\ngess.cfg syntax:biasesfmt=*str*'),
        'darkflatslib': _('If checked, you will need to specify the path to the masterdarkflats library and the name convention of your masterdarkflats'),
        'darkflats': _('Give the name of the folder that will contain the darkflats frames\nCould be DARKFLAT (NINA), darkflats (APT) etc... \nNot case sensitive on Win, Case sensitive on Linux/Mac\nIf you have checked \"I\'m using a library\", browse to the folder where you store yor masterdarkflats\n\ngess.cfg syntax:darkflats=*str*'),
        'darkflatsfmt': _('Give the name pattern for finding the masterdarkflats\nMasterdarkflats name is formed using values from the flats FITS header\ne.g. "DARKFLAT_[EXPOSURE:0.1f]_O[OFFSET:d]_bin[XBINNING:d].fit"\nIn the example above, value from key EXPOSURE will be converted to afloat with one-digit (as specified with the :0.1f)\n\ngess.cfg syntax:darkflatsfmt=*str*'),
        'copymasters':_('If checked, masters are hardcopied (not symlinked) into the masters subfolder'),
        'Load a light': _('Load a light file to show its FITS header keys'),
    }

    combodict={
        'ext':['fits','fit','fts','raw'],
        'shottype':['cfa','mono'],
        'bitdepth':['16','32']    
    }

    warndict={
        'Clear': _('You are about to clear all data in this form.\nDo you want to proceed?'),
        'Update Default': _('You are about to save all these values as the new default options\nDo you want to proceed?')
    }

    def updatelibs(libs,opt):
        for k in libs.keys():
            # if os.sep in opt[k.replace('lib','')]:
            if '\\' in opt[k.replace('lib','')] or '/' in opt[k.replace('lib','')]:
                libs[k]=True
        if libs.biaseslib & libs.darkflatslib:
            libs.darkflatslib=False
        return libs

    def _setcheckbox(key,opt,msgdict,tooltipsdict,disabled=False,enable_events=False):
        return sg.Checkbox(msgdict[key],opt[key],key=key,tooltip=tooltipsdict[key],disabled=disabled,enable_events=enable_events)

    def _setcombobox(key,opt,msgdict,tooltipsdict,combodict):
        return sg.Combo(combodict[key],opt[key],key=key,tooltip=tooltipsdict[key])


    def _libinput(name,opt,libs,msgdict,tooltipsdict):
        layout=[]
        layout+=[[sg.Text((name+' frames').capitalize(), font='Any 12')]]
        layout+=[[_setcheckbox(name+'lib',libs,msgdict,tooltipsdict,enable_events=True)]]
        layout+=[[sg.Text(msgdict[name],size=(17,1)),sg.Input(opt[name],key=name,tooltip=tooltipsdict[name],size=(66,1)),sg.FolderBrowse(target=name,key=name+'libbrowse',disabled=not(libs[name+'lib']),tooltip=tooltipsdict[name])]]
        layout+=[[sg.Text(msgdict[name+'fmt'], size=(17,1)),sg.Input(opt[name+'fmt'],key=name+'fmt',tooltip=tooltipsdict[name+'fmt'],disabled=not(libs[name+'lib']),size=(75,1))]]
        return layout

    def updateoptions(wd,opt):
        updict={}
        for k in opt.keys():
            if k in ['darks','flats','biases','darkflats']: #trying to get normalized path for folders
                if len(wd[k].Get())>0:
                    updict[k]=os.path.normpath(wd[k].Get())
                else:
                    updict[k]=wd[k].Get()
                wd[k].update(updict[k])
            else:
                updict[k]=wd[k].Get()
        return updict

    def warningwd(warntext):
        return sg.popup_ok_cancel(_('Warning')+': '+warntext,title='Warning')



    Opt=options('gess')
    opt=Opt.getoptions()
    libs=DictX()
    libs.darkslib=False
    libs.flatslib=False
    libs.biaseslib=False
    libs.darkflatslib=False


    process_layout = []
    process_layout+=[[sg.Text(_('Pre-processing options'), font='Any 12')]]
    process_layout+=[[_setcheckbox('ppdark',opt,msgdict,tooltipsdict)]]
    process_layout+=[[_setcheckbox('ppflat',opt,msgdict,tooltipsdict,enable_events=True)]]
    process_layout+=[[_setcheckbox('ppoffset',opt,msgdict,tooltipsdict,disabled=True)]]
    process_layout+=[[_setcheckbox('pplight',opt,msgdict,tooltipsdict,disabled=True)]]
    process_layout+=[[sg.HorizontalSeparator(pad=(0,10))]]
    process_layout+=[[sg.Text(_('Processing options'), font='Any 12')]]
    process_layout+=[[_setcheckbox('dobkg',opt,msgdict,tooltipsdict)]]
    process_layout+=[[_setcheckbox('doHO',opt,msgdict,tooltipsdict)]]
    process_layout+=[[_setcheckbox('doregister',opt,msgdict,tooltipsdict)]]
    process_layout+=[[_setcheckbox('dostack',opt,msgdict,tooltipsdict)]]

    preferences_layout=[]
    preferences_layout+=[[sg.Text('Images', font='Any 12')]]
    preferences_layout+=[[sg.Text('Extension:', size=(10,1)),_setcombobox('ext',opt,msgdict,tooltipsdict,combodict)]]
    preferences_layout+=[[sg.Text('Shot type:', size=(10,1)),_setcombobox('shottype',opt,msgdict,tooltipsdict,combodict)]]
    preferences_layout+=[[sg.Text('Bitdepth:', size=(10,1)),_setcombobox('bitdepth',opt,msgdict,tooltipsdict,combodict)]]
    preferences_layout+=[[_setcheckbox('seqasfitseq',opt,msgdict,tooltipsdict)]]
    preferences_layout+=[[sg.Text(msgdict['compression'], size=(10,1)),sg.Input(opt['compression'],key='compression',tooltip=tooltipsdict['compression']),sg.Button('Help',key='compressionhelp')]]

    preferences_layout+=[[sg.HorizontalSeparator(pad=(0,10))]]
    preferences_layout+=[[sg.Text('General', font='Any 12')]]
    preferences_layout+=[[_setcheckbox('debug',opt,msgdict,tooltipsdict)]]
    preferences_layout+=[[sg.Text(msgdict['sirilexe'], size=(10,1)),sg.Input(opt['sirilexe'],key='sirilexe',tooltip=tooltipsdict['sirilexe']), sg.FileBrowse(target='sirilexe',tooltip=tooltipsdict['sirilexe'])]]


    folders_layout=[]
    folders_layout+=[[sg.Text('Light frames', font='Any 12')]]
    folders_layout+=[[sg.Text(msgdict['lights'], size=(17,1)),
                      sg.Input(opt['lights'],key='lights',tooltip=tooltipsdict['lights']),
                    #   sg.FileBrowse('Load a light',file_types = (('fits', '*.fits'),('fit', '*.fit'),('fts', '*.fts'),),tooltip=tooltipsdict['Load a light'])
                      ]]
    folders_layout+=[[sg.Text(msgdict['lightsfmt'], size=(17,1)),
                    sg.Input(opt['lightsfmt'],key='lightsfmt',tooltip=tooltipsdict['lightsfmt'],size=(75,1)),
                #   sg.FileBrowse('Load a light',file_types = (('fits', '*.fits'),('fit', '*.fit'),('fts', '*.fts'),),tooltip=tooltipsdict['Load a light'])
                    ]]
    
    folders_layout+=[[sg.HorizontalSeparator(pad=(0,10))]]


    folders_layout+=_libinput('darks',opt,libs,msgdict,tooltipsdict)
    folders_layout+=_libinput('flats',opt,libs,msgdict,tooltipsdict)
    folders_layout+=_libinput('biases',opt,libs,msgdict,tooltipsdict)
    folders_layout+=_libinput('darkflats',opt,libs,msgdict,tooltipsdict)
    folders_layout+=[[sg.HorizontalSeparator(pad=(0,10))]]
    folders_layout+=[[_setcheckbox('copymasters',opt,msgdict,tooltipsdict)]]


                        
    layout=[[
            sg.TabGroup([[
                            sg.Tab(_('Preferences'), preferences_layout), 
                            sg.Tab(_('Processing'), process_layout),   
                            sg.Tab(_('Folders'), folders_layout)
                            ]])
            ]]
    buttonsize=11
    if action=='set':
        layout+=[[          
                sg.Button(_('Clear'),tooltip=_('Reset current options to default values'),size=(buttonsize,1)),
                    sg.Button(_('Load'),tooltip=_('Load an existing cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Update Default'),tooltip=_('Update GeSS default cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Save As'),tooltip=_('Save as an optional cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Print'),tooltip=_('Print options'),size=(buttonsize,1)),
                    sg.Button(_('Test'),tooltip=_('Test myoptions'),size=(buttonsize,1)),
                ]]
    elif action=='inter':
        layout+=[[          
                    sg.Button(_('Load'),tooltip=_('Load an existing cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Save As'),tooltip=_('Save as an optional cfg file'),size=(buttonsize,1)),
                    sg.Button(_('Validate'),tooltip=_('Pass the options to gess and go on with processing the session'),size=(buttonsize,1)),
                ]]        
   



    window = sg.Window(_('GeSS options wizard'), layout, default_element_size=(50,2),finalize=True)    
    libs=updatelibs(libs,opt)
    for k in libs.keys():
        window[k].Update(libs[k])  
        window[k+'browse'].Update(disabled=not(libs[k]))
        window[k.replace('lib','fmt')].Update(disabled=not(libs[k]))

    while True: 

        event, values = window.read()  

        if event == sg.WIN_CLOSED:   
            break        # always,  always give a way out!    
               

        if event==_('Clear'):
            popup=warningwd(warndict['Clear'])
            if popup.lower()=='ok':
                Opt=options(returnclean=True)
                opt=Opt.getoptions()
                for k in opt.keys():
                    window[k].Update(value=opt[k])
                for k in libs.keys():
                    libs[k]=False
                    window[k].Update(libs[k])  
                    window[k+'browse'].Update(disabled=not(libs[k]))
                    window[k.replace('lib','fmt')].Update(disabled=not(libs[k]))  

        if event==_('Load'):
            options.importcfgfile(Opt,update=False)
            opt=Opt.getoptions()
            for k in opt.keys():
                window[k].Update(value=opt[k])
            libs=updatelibs(libs,opt)
            for k in libs.keys():
                window[k].Update(libs[k])  
                window[k+'browse'].Update(disabled=not(libs[k]))
                window[k.replace('lib','fmt')].Update(disabled=not(libs[k]))  

        if event==_('Update Default'):
            popup=warningwd(warndict['Update Default'])
            if popup.lower()=='ok':
                Opt=options(returnclean=True)
                opt=Opt.getoptions()
                newvals=updateoptions(window,opt)
                Opt=options(dictcfg=newvals,updatedefault=True)

        if event==_('Save As'):
            Opt=options(returnclean=True)
            opt=Opt.getoptions()
            newvals=updateoptions(window,opt)
            Opt=options(dictcfg=newvals,updatedefault=False)
            Opt.exportcfgfile()

        

        if event=='ppflat':
            window['ppoffset'].update(value=values['ppflat'])

        if event=='compressionhelp':
            import webbrowser
            webbrowser.open('https://free-astro.org/index.php/Siril:Commands#setcompress') 

            
        if 'lib' in event:
            for k in libs.keys():
                libs[k]=bool(window[k].Get())

            if libs.biaseslib & libs.darkflatslib:
                if event=='darkflatslib':
                    libs.biaseslib=False
                if event=='biaseslib':
                    libs.darkflatslib=False
            for k in libs.keys():      
                window[k].Update(libs[k])  
                window[k+'browse'].Update(disabled=not(libs[k]))
                window[k.replace('lib','fmt')].Update(disabled=not(libs[k]))            

        if event==_('Print'):
            for k in opt.keys():
                if k in values.keys():
                    print(k+':'+str(values[k]))

        if event==_('Test'):
            #make sure the options are updated
            Opt=options(returnclean=True)
            opt=Opt.getoptions()
            newvals=updateoptions(window,opt)
  
            success,res=gessi.Run(addopt=newvals,dryrun=True,fromgui=True)
            if success:
                msg=sg.popup_ok_cancel(_('Congrats'), _('You did it!\nDo you want to save these options?'))
                if msg.lower()=='ok':
                    window[_('Save As')].Click()
            else:
                sg.popup_error(_('Ooops...'), _('Something went wrong\nDryrun terminated with the following error:\n\n\"{:s}\"\n\nCorrect and test again').format(res['message']),line_width=50)

        if event==_('Validate'):
            #make sure the options are updated
            Opt=options(returnclean=True)
            opt=Opt.getoptions()
            newvals=updateoptions(window,opt)
            Opt=options(optiontype='gess',dictcfg=newvals)
            window.close()
            return Opt.getoptions()
            
    return None

if __name__ == "__main__":

    Run()