import os,sys

if __package__ is None or len(__package__)==0: # for debugging purposes only
    sys.path.append(".")

from gess import gessengine,gessi,gessmultisession,gessloop,gesscopyback
from gess.utils import SSFy,GUI_options,GUI_loopoptions,GUI_copybackoptions
from gess.common.options import options
from gess.common.i18n import _

#TODO: copyback, loop with copyback


ALLOWED_OPTIONS=['-c','-e','-ed','-h','-i','-o','-ol','-oc','-l','-m','-s']
help=[]
help+=[_('Welcome to GeSS - here below a guide start guide to the different calls and their arguments:')]
help+=[_('Note: arguments between brackets are optional\n')]
help+=[_('gess -o -> starts GeSS options wizard\nCorrectly filling these options is mandatory for all the other calls to work\nYou can also call this module to define other sets of options if you have multiple set-ups etc...\n')]
help+=[_('gess -ol -> starts GeSS loop options wizard\nCorrectly filling these options is mandatory for all the other calls to gess loop to work\nYou can also call this module to define other sets of options if you have multiple set-ups etc...\n')]
help+=[_('gess -oc -> starts GeSS copyback options wizard\nCorrectly filling these options is mandatory for all the other calls to copyback to work\nYou can also call this module to define other sets of options if you have multiple set-ups etc...\n')]
help+=[_('gess -i -> starts gess in interactive mode\nYou\'ll be able to review/modify the options and then select the working directory\n')]
help+=[_('gess -l -> starts gess in loop mode\ngess will loop through all the sessions run during previous night\nRequires to have set loop.cfg file\n')]
help+=[_('gess -l [addgesscfg=addgess.cfg] [addloopcfg=addloop.cfg] [searchstr=2021-01-15]-> starts gess in loop mode\ngess will loop through all the sessions run at date specified by searchstr\ngess.cfg and loop.cfg are overloaded by the options given in addgess.cfg and addloop.cfg respectively\n')]
help+=[_('gess -m -> starts gess in multisession mode\nSelect multiple folders containing calibrated lights and it will gather, register and stack them\n')]
help+=[_('gess -e workdir [addcfgfile=addgess.cfg] [opt1=val1] [opt2=val2]-> launches gess engine in the working directory workdir\noverloading default options with options in addgess.cfg and then options specified by their names with a mandatory \'=\' sign\n')]
help+=[_('gess -s utility to \"SSFy\" a log from a gess session. This module extracts all the successful commands that were passed to Siril.\nUseful to modify and rerun with Siril, or simply to remove all the verbose from logs and review the actions that were performed.\n')]
help+=[_('gess -c utility to copy back the calibrated lights from a session to another folder upwards on your path.\nThis module can be used to gather all calibrated lights from different sessions, following gess loop inputs.\n')]

# help+=['gess -ed workdir [addgess.cfg] [opt1=val1] [opt2=val2]->same as above but in dry-run mode.\nThe process stops after all the preliminary checks (folders are found, Siril can start and it accepts all the options)\n']



def main():

    try:
        module=sys.argv[1]
    except:
        module='0'

    if not (module in ALLOWED_OPTIONS):
        print(_('{:s} is not a valid gess option').format(module))
        module='-h'

    if module=='-h':
        for h in help:
            print(h)

    if module.startswith('-e'):
        addcfgfile=''
        optdict={}
        for a in sys.argv[3:]:
            if '=' in a:
                f,v=a.split('=')
                if not '.cfg' in v:
                    optdict[f]=v
                else:
                    addcfgfile=v
            else:
                addcfgfile=a
        opt=options(addcfgfile=addcfgfile,dictcfg=optdict).getoptions()    
        if 'd' in module: # undocumented
            success,res=gessengine.Run(sys.argv[2],opt,dryrun=True)
        else:
            success,res=gessengine.Run(sys.argv[2],opt)

    if module=='-i':
        opt=GUI_options.Run('inter')
        if not (opt is None):
            success,res=gessi.Run(addopt=opt,dryrun=False,fromgui=True)
        else:
            print(_('Interactive session aborted'))

    if module.startswith('-o'):
        if 'l' in module:
            success=GUI_loopoptions.Run()
        elif 'c' in module:
            success=GUI_copybackoptions.Run()
        else:
            success=GUI_options.Run('set')

    if module=='-l':
        args=[]
        kwargs={}
        for a in sys.argv[2:]:
            if '=' in a:
                f,v=a.split('=')    
                kwargs[f]=v
            else:
                args.append(a)
        success,res=gessloop.Run(*tuple(args),**kwargs)

    if module=='-c':
        args=[]
        kwargs={}
        for a in sys.argv[2:]:
            if '=' in a:
                f,v=a.split('=')    
                kwargs[f]=v
            else:
                args.append(a)
        success,res=gesscopyback.Run(*tuple(args),**kwargs)

    if module=='-m':
        args=[]
        kwargs={}
        for a in sys.argv[2:]:
            if '=' in a:
                f,v=a.split('=')    
                kwargs[f]=v
            else:
                args.append(a)
        success,res=gessmultisession.Run(*tuple(args),**kwargs)


    if module=='-s':
        success,res=SSFy.Run()
    

    try:
        logstr=res['log']
        print('Log saved at {:s}'.format(logstr))
    except:
        pass
        
    return True







if __name__ == "__main__":

    main()