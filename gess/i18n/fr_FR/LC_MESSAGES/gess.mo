��    �          �         <        >     T     j     s     �  B   �     �  ?   �     *     D     T    `     n     w  "   �  3   �  -   �  +     -   H  2   v  0   �  2   �       C   -     q     �  !   �  '   �  ;   �  L   !  >   n  >   �  1   �  #     '   B     j  (   �     �     �  U   �  9   '     a    g  &   i     �     �     �  �   �  0   S     �     �     �  F   �     	  #   #      G     h     }     �  �   �  �   �  �   �  �   �    �    �  �   �   �   �!  �   "  (  #  W  C$  N  �%  0  �&  a   (     }(     �(     �(     �(  �   �(  g   �)  S   &*  V   z*  �   �*  �   �+  v   t,  |   �,  t   h-  t   �-  �   R.  %   4/     Z/     v/  .   {/     �/     �/     �/     �/     0     "0     70  B   M0  1   �0  &   �0     �0     �0  .   1     D1  "   M1  >   p1     �1     �1  +   �1     2     2  1   $2     V2     h2     �2  .   �2     �2     �2  
   �2     �2  9   3  '   =3     e3     m3     �3      �3  8   �3  ;   4     ?4     ]4  >   u4  4   �4     �4     5     %5     45  �  T5     �6     	7     7  
   )7     47     97  M   H7  /   �7  ?   �7     8  &   "8  M   I8  "   �8     �8     �8     �8     �8  \   �8  I   T9  -  �9  E   �:  Y   ;  '   l;  U   �;  .   �;  6   <  �   P<  }   F=  �   �=    N>  �   l?  �   �?    �@     �A     �A  =   B  6   MB  -   �B  P   �B  )   C  2   -C  .   `C     �C  &   �C  !   �C  5   �C     !D  3   6D     jD  P  �D  F   �E  "   "F  )   EF     oF  $   wF     �F  E   �F  
   �F  I   �F  &   IG     pG     �G  K  �G     �H  ,   �H  /   !I  ;   QI  2   �I  2   �I  0   �I  2   $J  2   WJ  0   �J  /   �J  Y   �J      EK     fK  /   �K  9   �K  H   �K  [   9L  M   �L  M   �L  7   1M  .   iM  4   �M  )   �M  >   �M     6N     KN  A   \N  >   �N  	   �N  +  �N  +   P     ?P     HP     YP  �   fP  2   QQ     �Q  #   �Q     �Q  ^   �Q  %   8R  -   ^R  1   �R     �R  !   �R     �R    S    "T  �   +U    $V    0W    LX    fY    lZ  �   x[  +   \  s  ,]  ^  �^  6  �_  `   6a  %   �a  (   �a  $   �a      b  �   ,b  }    c  [   ~c  X   �c    3d  �   8e  |   �e  �   |f  z   �f  z   zg    �g  $   �h     i     :i  8   Bi  %   {i     �i     �i      �i     �i     j     8j  E   Vj  7   �j  /   �j  !   k  !   &k  2   Hk     {k  &   �k  F   �k     �k     l  >   l     ^l     xl  =   �l     �l  (   �l  (   m  ;   0m  	   lm     vm  
   �m     �m  ;   �m  4   �m     n  /   -n  3   ]n  /   �n  =   �n  E   �n  $   Eo  "   jo  V   �o  H   �o     -p  (   Kp     tp  (   �p  �  �p  *   }r     �r     �r     �r     �r     �r  Z   �r  9   Ms  Q   �s     �s  -   �s  ^   !t  #   �t  #   �t  0   �t     �t     u  d   u  M   tu  ^  �u  Y   !w  y   {w  1   �w  b   'x  7   �x  J   �x  1  y  �   ?z  v   �z  !  <{  �   ^|    }    ~  &   /     V  :   s  ?   �  /   �  p   �  6   ��  ?   ƀ  :   �     A�  3   [�  '   ��  O   ��     �  H   �  %   b�             i      ^   	   !   :          @      Q   �   h   �   �   s                   E   X   �      y                      �   t   D       �   U   2       ~   �          7   )   3      H              $               #           �   �       W   G         9   ]   r      �   z   d   u   4   ,           �   j      -       �       [   �       `   �   I   �   �   Z       A       �       F   K       �       l   /   �   �          k   �                  �       �   �   �   g   �   1   �   &      �   |           �              �   �   
   �              T   q      �   �   �   5   O       c   o   _   P   a       �   �   N   8                      n   �   <   S         \   (   M       L           '   =       �   ;      "   m   }       �   �   �   %       �   �   �       �   �       �   w   �   J   B   �      �   .               �   R   C         �         0   e   Y   �          �   �       �   ?   �   f              x   +       p   >   *   6   �   �   {   �   b   �   V           �   v        
Searching for all folders under {0:s} containing date {1:s}  successfully stacked * GeSS loop summary * Aborting Assemble fits as a fitseq file Biases format Bitdepth of the processed images

gess.cfg syntax:bitdepth=16/[32] Cancel Choose cfa (color) or mono

gess.cfg syntax:shottype=cfa/[mono] Choose configuration file Choose log file Compression Compression string as defined in:
https://free-astro.org/index.php/Siril:Commands#setcompress 
You should omit the setcompress string e.g. simply enter 0 for no compression
Leave blank if you want to apply Siril settings

gess.cfg syntax:compression=[0/1 -type=rice 16] Congrats Convert command failed for {:s} Convertraw command failed for {:s} Copied file and original header from {0:s} to {1:s} Could not convert your entry to boolean value Could not convert your entry to float value Could not convert your entry to integer value Could not convert your entry {:s} to boolean value Could not convert your entry {:s} to float value Could not convert your entry {:s} to integer value Could not find a flat matching: Could not find any subfolder or library containing biases/darkflats Could not find darks Could not find flats Could not find folder containing  Could not find folder containing lights Could not find suitable master: {:s} in your master library Could not find {0:s} key in the FITS header of {1:s}, please check and retry Could not load the optional gess configuration file - aborting Could not load the optional loop configuration file - aborting Could not read the additional cfg file - aborting Could not set bitdepth to {:d} bits Could not set current directory to {:s} Could not set extension to {:s} Creating and filling subset folder {0:s} Darkflats format Darks format Define the path where all your sessions are stored.

loop.cfg syntax:imagingfolder="" Did not find same number of Ha and OIII images - aborting Done
 Extension of image files as saved by your imaging software
If raw, gess will call Siril convertraw command instead of convert - to be used only if you save other file types(e.g *.jpg) together with your DSLR raw files

gess.cfg syntax:ext=fits/[fit/fts/raw] Extract background before registration Finished Flats format Folders For advanced users only! This option can be used to point to a dev version
Normal case: Leave blank to use default Siril location

gess.cfg syntax:sirilexe=somepath Found more than one file matching pattern :{0:s} Found option {0:s} Found {0:d} + {1:d} files Found {:d} files Found {:d} sets of files - Type the number of the set you want to use: Gather files and register Gather files from multiple sessions Gather files, register and stack Gathering {:d} files GeSS loop options wizard GeSS options wizard Give the name of the folder that will contain the bias frames to stack
Once stacked, all the masters will be stored in your biases library

loop.cfg syntax:biases2lib=""
Note: if left blank, gessloop will not look for biases to fill your library Give the name of the folder that will contain the biases frames
Could be BIAS (NINA), biases (APT) etc... Not case sensitive
If you have checked "I'm using a library", browse to the folder where you store yor masterbiases

gess.cfg syntax:biases=*str* Give the name of the folder that will contain the dark frames
Could be DARK (NINA), darks (APT) etc... Not case sensitive
If you have checked "I'm using a library", browse to the folder where you store yor masterdarks

gess.cfg syntax:darks=*str* Give the name of the folder that will contain the dark frames to stack
Once stacked, all the masters will be stored in your darks library

loop.cfg syntax:darks2lib=""
Note: if left blank, gessloop will not look for darks to fill your library Give the name of the folder that will contain the darkflat frames to stack
Once stacked, all the masters will be stored in your darkflats library

loop.cfg syntax:darkflats2lib=""
Note: if left blank, gessloop will not look for darkflats to fill your library Give the name of the folder that will contain the darkflats frames
Could be DARKFLAT (NINA), darkflats (APT) etc... Not case sensitive
If you have checked "I'm using a library", browse to the folder where you store yor masterdarkflats

gess.cfg syntax:darkflats=*str* Give the name of the folder that will contain the flat frames
Could be FLAT (NINA), flats (APT) etc... Not case sensitive
If you have checked "I'm using a library", browse to the folder where you store yor masterflats

gess.cfg syntax:flats=*str* Give the name of the folder that will contain the flat frames to stack
Once stacked, all the masters will be stored in your flats library

loop.cfg syntax:flats2lib=""
Note: if left blank, gessloop will not look for flats to fill your library Give the name of the folder that will contain the light frames
Could be LIGHT (NINA), lights (APT) etc... Not case sensitive

gess.cfg syntax:lights=*str* Give the name pattern for finding the masterbiases
Masterbias name is formed using values from the flats FITS header
e.g. "BIAS_O[OFFSET:d]_bin[XBINNING:d].fit"
In the example above, value from key OFFSET will be converted to an integer (as specified with the :d)

gess.cfg syntax:biasesfmt=*str* Give the name pattern for finding the masterdarkflats
Masterdarkflats name is formed using values from the flats FITS header
e.g. "DARKFLAT_[EXPOSURE:0.1f]_O[OFFSET:d]_bin[XBINNING:d].fit"
In the example above, value from key EXPOSURE will be converted to afloat with one-digit (as specified with the :0.1f)

gess.cfg syntax:darkflatsfmt=*str* Give the name pattern for finding the masterdarks
Masterdark name is formed using values from the lights FITS header
e.g. DARK_[EXPTIME:d]s_G[GAIN:d]_O[OFFSET:d]_T[SET-TEMP:d]C_bin[XBINNING:d].fits
In the example above, value from key EXPTIME will be converted to an interger (as specified with the :d)

gess.cfg syntax:darksfmt=*str* Give the name pattern for finding the masterflats
Masterflat name is formed using values from the lights FITS header
e.g. FLAT_[TELESCOP:s]_[FILTER:s]_bin[XBINNING:d].fits
In the example above, value from key FILTER will be converted to a string (as specified with the :s)

gess.cfg syntax:flatsfmt=*str* Give the string to parse the date format
Example ""%Y-%m-%d"

loop.cfg syntax:datefmt=""%Y-%m-%d" I'm using a biases library I'm using a darkflats library I'm using a darks library I'm using a flats library If checked, gess and its modules will keep logs of their actions
It is strongly suggested to keep this option ticked.
Otherwise, it will not be possible to get any assistance.

gess.cfg syntax:debug=true/[false] If checked, gess will call Siril background extraction with order 1

gess.cfg syntax:dobkg=true/[false] If checked, gess will call Siril extract Ha/OIII

gess.cfg syntax:doHO=false/[true] If checked, gess will call Siril registration

gess.cfg syntax:doregister=true/[false] If checked, lights will be divided by masterflat.
If you're not using a flats library, flat frames will be stacked before lights preprocessing

gess.cfg syntax:ppflat=true/[false]
Note: if ppflat=true, then ppoffset=true in gess.cfg If checked, masterdark will be susbtracted from lights.
If you're not using a darks library, dark frames will be stacked before lights preprocessing

gess.cfg syntax:ppdark=true/[false] If checked, you will need to specify the path to the masterbiases library and the name convention of your masterbiases If checked, you will need to specify the path to the masterdarkflats library and the name convention of your masterdarkflats If checked, you will need to specify the path to the masterdarks library and the name convention of your masterdarks If checked, you will need to specify the path to the masterflats library and the name convention of your masterflats If multiple sets of lights can be stored in the same light folder, give a convention to rename every set
Example: "LIGHT_F[FILTER:s]_[EXPTIME:d]s_G[GAIN:d]_bin[XBINNING:d]" without extension.

loop.cfg syntax:lightsfolders="" If valid, the option value is updated Interactive session aborted Load Load a light file to show its FITS header keys Load an existing cfg file Master{0:s} found @ {1:s} Name of biases folder Name of darkflats folder Name of darks folder Name of flats folder Name of lights folder New version of master{0:s} {1:s} has been copied to library: {2:s} No commands to be SSFied from this log - aborting No {:s} folder found matching the date None found - Aborting None found - aborting Note: arguments between brackets are optional
 Ooops... Option number is not valid - retry Pass the options to gess and go on with processing the session Perform registration Perform stacking Please select the session working directory Pre-processing options Preferences Preprocess flats with offsets (bias or darkflats) Preprocess lights Preprocess lights with darks Preprocess lights with flats Press Enter on an empty line when you are done Print Print options Processing Processing options Reconsider your naming convention - Using the first found Reset current options to default values Save As Save as an optional cfg file Save configuration file as...  Save logs (Strongly recommended) Searching for all folders under {0:s} containing : {1:s} Searching for all folders under {0:s} containing date {1:s} Searching for {:s} subfolders Select another location Select folders containing calibrated lights - Cancel when done Select/create folder to store the multisession merge Separate Ha and OIII layers Set number is not valid - retry Siril location Skipping this folder processing Special key for NINA users. When filled, it will tell gessloop that flats can be stored elsewhere than together with the lights and how to assign them correctly to each light.
Example: "FLAT_[FILTER:s]_O[OFFSET:d]_bin[XBINNING:d]" without extension.

loop.cfg syntax:NINAflatwizard=""
Note: It is somewhat similar to using a flats library except the masterflats won't be persistent in between your sessions Stack command failed for {:s} Starting Siril Starting pySiril Successful Test Test myoptions The specified master library path {:s} does not exist, please check and retry There was a problem when copying the {:s} files There was a problem when copying the {:s} files - check the log This option does not exists This option does not exists - ignoring Type the option name and updated value, separated by a = sign and press Enter Unknown mode for parsemasterformat Update Default Update GeSS default cfg file Validate Warning Welcome to GeSS - here below a guide start guide to the different calls and their arguments: What do you want to do next ? Type the number of one of the options below When checked, masteroffset will be substracted from flats before stacking.
If you're not using an offsets library, offset frames will be stacked before flats preprocessing

gess.cfg syntax:ppoffset=true/[false]
Note: ppoffset value is determmined by ppflat value, this checkbox value cannot be changed You are about to clear all data in this form.
Do you want to proceed? You are about to save all these values as the new default options
Do you want to proceed? You can change any of the options above You cannot choose one of the original process folders to store the multisession merge You did it!
Do you want to save these options? background extraction command failed for sequence {:s} gess -e workdir [addcfgfile=addgess.cfg] [opt1=val1] [opt2=val2]-> launches gess engine in the working directory workdir
overloading default options with options in addgess.cfg and then options specified by their names with a mandatory '=' sign
 gess -i -> starts gess in interactive mode
You'll be able to review/modify the options and then select the working directory
 gess -l -> starts gess in loop mode
gess will loop through all the sessions run during previous night
Requires to have set loop.cfg file
 gess -l [addgesscfg=addgess.cfg] [addloopcfg=addloop.cfg] [searchstr=2021-01-15]-> starts gess in loop mode
gess will loop through all the sessions run at date specified by searchstr
gess.cfg and loop.cfg are overloaded by the options given in addgess.cfg and addloop.cfg respectively
 gess -m -> starts gess in multisession mode
Select multiple folders containing calibrated lights and it will gather, register and stack them
 gess -o -> starts GeSS options wizard
Correctly filling these options is mandatory for all the other calls to work
You can also call this module to define other sets of options if you have multiple set-ups etc...
 gess -s utility to "SSFy" a log from a gess session. This module extracts all the successful commands that were passed to Siril.
Useful to modify and rerun with Siril, or simply to remove all the verbose from logs and review the actions that were performed.
 gess default options from {:s} gess multisession run failed gess.cfg needs to specify the format to name the {:s} masters gess.cfg needs to specify the path to the {:s} library levelup cannot be null or negative - aborting loop.cfg needs to specify a simple string, not a path, where the {:s} are stored register command failed for sequence {:s} seqextract_HaOIII command failed for sequence {:s} setcompress command failed with arguments {:s} ssf saved at: {:s} stack command failed for sequence {:s} with additional options from {:s} {0:s} from {1:s} could not be stacked - check the log {:s} does not exists {:s} has spaces and cannot be used to build scripts {:s} is not a valid gess option Project-Id-Version: 
POT-Creation-Date: 2021-01-29 04:51+0100
PO-Revision-Date: 2021-01-29 05:39+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n > 1);
 
Recherche de tous les répertoires sous {0:s} contenant la date {1:s}  empilement effectué avec succès * Resumé de la configuration GeSS loop * Abandon Assemblage de fits en fichier fitseq Format des biases Nombre de bit des images traitées

gess.cfg syntaxe:bitdepth=16/[32] Annulation Choisir entre cfa (couleur) or mono

gess.cfg syntaxe:shottype=cfa/[mono] Sélection du fichier de configuration Sélection du fichier de log Compression Chaîne de caractère définissant la compression tel que définie à l'adresse:
https://free-astro.org/index.php/Siril:Commands#setcompress 
Vous devez omettre la chaîne setcompress. Par exemple entrez 0 pour pas de compression
Laissez vide pour utiliser les paramètres de Siril

gess.cfg syntaxe:compression=[0/1 -type=rice 16] Félicitations La commande convert a échoué pour les {:s} La commande convertraw a échoué pour les {:s} Fichier et entête original copiés depuis {0:s} vers {1:s} Impossible de convertir l'entrée {:s} en booléen Impossible de convertir l'entrée {:s} en flottant Impossible de convertir l'entrée {:s} en entier Impossible de convertir l'entrée {:s} en booléen Impossible de convertir l'entrée {:s} en flottant Impossible de convertir l'entrée {:s} en entier Impossible de trouver un flat correspondant à: Impossible de trouver un sous-répertoire ou une librairie contenant les biases/darkflats Impossible de trouver les lights Impossible de trouver les flats Impossible de trouver le répertoire contenant  Impossible de trouver le répertoire contenant les lights Impossible de trouver un master adapté dans la bibliothèques de master Impossible de trouver l'entrée {0:s} dans l'entête FITS de {1:s}, vérifiez et réessayez Impossible de charger le fichier de configuration optionnel de gess - abandon Impossible de charger le fichier de configuration optionnel de loop - abandon Impossible de lire le fichier cfg additionnel - abandon Impossible de changer le bitdepth à {:d} bits Impossible de changer le répertoire courant à {:s} Impossible de changer l'extension en {:s} Création et remplissage du répertoire de sous-ensemble {0:s} Format des darkflats Format des darks Chemin de toutes vos sessions.

loop.cfg syntaxe:imagingfolder="" Les nombres d'images Ha et OIII trouvées diffèrent - abandon Terminé
 Extension des images issues de votre logiciel d'imagerie
Si raw, gess appellera la commande convertraw de Siril à la place de la commande convert - à n'utiliser que si vous sauvegardez d'autres types de fichier (ex: *.jpg) en plus des fichiers raw de l'APN

gess.cfg syntaxe:ext=fits/[fit/fts/raw] Extraction du fond de ciel avant alignement Terminé Format des flats Répertoires Pour les utilisateurs avancés uniquement! Cette option peut être utilisée pour pointer vers un version de développement
Cas usuel: laisser vider pour utiliser l'emplacement de Siril par défaut

gess.cfg syntaxe:sirilexe=un chemin Plusieurs fichiers correspondent au patron :{:0:s} Option {:0s} fournie {0:d} + {1:d} fichier(s) trouvé(s) {0:d} fichier(s) trouvé(s) {:d} ensembles de fichier trouvés - Entrez le numéro de l'ensemble que vous voulez utiliser: Collection des fichiers et alignement Collection des fichiers de sessions multiples Collection des fichiers, alignement et empilement Collection de {:d} fichers Assistant d'option pour GeSS loop Assistant d'option de GeSS Nom du répertoire qui contiendra les bias frames à empiler
Une fois empilés, tous les masters seront enregistrés dans la bibliothèque de bias

loop.cfg syntaxe:biases2lib=""
Note: si laisser vide, gessloop ne recherchera pas de bias pour remplir la bibliothèque Nom du répertoire qui contiendra les biases
Peut être BIAS (NINA), biases (APT) etc... Non sensible à la case
Si vous avez coché "J'utilise une librairie", parcourez et sélectionnez le répertoire ou vous stockez vos masterbiase

gess.cfg syntaxe:biases=*str* Nom du répertoire qui contiendra les darts
Peut être DARK (NINA), darks (APT) etc... Non sensible à la casse
Si avez coché "J'utilise une bibliothèque", sélectionnez le répertoire ou vous stockez vos masterdarks

gess.cfg syntaxe:darks=*str* Nom du répertoire qui contiendra les dark frames à empiler
Une fois empilés, tous les masters seront enregistrés dans la bibliothèque de darks

loop.cfg syntaxe:darks2lib=""
Note: si laisser vide, gessloop ne recherchera pas de dark pour remplir la bibliothèque Nom du répertoire qui contiendra les darkflat frames à empiler
Une fois empilés, tous les masters seront enregistrés dans la bibliothèque de darkflats

loop.cfg syntaxe:darkflats2lib=""
Note: si laisser vide, gessloop ne recherchera pas de darkflat pour remplir la bibliothèque Nom du répertoire qui contiendra les darkflats
Peut être DARKFLAT (NINA), darkflats (APT) etc... Non sensible à la case
Si vous avez coché "J'utilise une librairie", parcourez et sélectionnez le répertoire ou vous stockez vos masterdarkflats

gess.cfg syntaxe:darkflats=*str* Nom du répertoire qui contiendra les flats
Peut être FLAT (NINA), flats (APT) etc... Non sensible à la case
Si vous avez coché "J'utilise une librairie", parcourez et sélectionnez le répertoire ou vous stockez vos masterflats

gess.cfg syntaxe:flats=*str* Nom du répertoire qui contiendra les flat frames à empiler
Une fois empilés, tous les masters seront enregistrés dans la bibliothèque de flats

loop.cfg syntaxe:flats2lib=""
Note: si laisser vide, gessloop ne recherchera pas de flat pour remplir la bibliothèque Nom du répertoire contenant les lights
Peut être LIGHT (NINA), lights (APT)... Non sensible à la casse

gess.cfg syntax:lights=*str* Fourni le patron de nommage des masterbiases
Le nom du masterbias est dérivé des valeurs des entêtes des fichiers FITS des flats
ex: "BIAS_O[OFFSET:d]_bin[XBINNING:d].fit"
Dans l'exemple ci-dessus, la valeur OFFSET est convertie en entier (comme indiqué par :d)

gess.cfg syntaxe:biasesfmt=*str* Fourni le patron de nommage des masterdarkflats
Le nom du masterdarkflats est dérivé des valeurs des entêtes des fichiers FITS des flats
ex: "DARKFLAT_[EXPOSURE:0.1f]_O[OFFSET:d]_bin[XBINNING:d].fit"
Dans l'exemple ci-dessus, la valeur EXPOSURE est convertie en flottant avec un seul chiffre significatif (comme indiqué par :0.1f)

gess.cfg syntaxe:darkflatsfmt=*str* Fourni le patron de nommage des masterdarks
Le nom du masterdark est formé à partir des valeurs de l'entête FITS des lights
e.g. DARK_[EXPTIME:d]s_G[GAIN:d]_O[OFFSET:d]_T[SET-TEMP:d]C_bin[XBINNING:d].fits
Dans l'exemple ci-dessus, la valeur du paramètre EXPTIME sera converti en entier (tel que spécifié par :d)

gess.cfg syntaxe:darksfmt=*str* Fourni le patron de nommage des masterflats
Le nom du masterflat est dérivé des valeurs des entêtes des fichiers FITS des lights
ex: FLAT_[TELESCOP:s]_[FILTER:s]_bin[XBINNING:d].fits
Dans l'exemple ci-dessus, la valeur FILTER est convertie en chaîne (comme indiqué par :s)

gess.cfg syntaxe:flatsfmt=*str* Chaîne définissant le format de date
Example ""%Y-%m-%d"

loop.cfg syntaxe:datefmt=""%Y-%m-%d" J'utilise une bibliothèque de biases J'utilise une bibliothèque de darkflats J'utilise une bibliothèque de darks J'utilise une librairie de flats Si coché, gess et ses modules loggeront leurs actions
Il est fortement recommandé de gardé cette option cochée
Sans cela, il ne sera pas possible d'obtenir de l'assistance

gess.cfg syntax:debug=true/[false] Si coché, gess appellera l'extraction de fond de ciel de Siril avec un ordre égal à 1

gess.cfg syntaxe:dobkg=true/[false] Si coché, gess appellera l'extraction Ha/OIII de Siril

gess.cfg syntaxe:doHO=false/[true] Si coché, gess appelera l'alignement de Siril

gess.cfg syntaxe:doregister=true/[false] Si coché, les lights seront divisés par le masterflat
Si vous n'utilisez pas de bibliothèque de flats, les flats seront empilés avant le prétraitement des lights

gess.cfg syntaxe:ppflat=true/[false]
Note: si ppflat=true, alors ppoffset=true dans gess.cfg Si coché, le materdark sera soustrait des lights
Si vous n'utilisez pas de bibliothèque de darks, les darks seront empilés avant le prétraitement des lights

gess.cfg syntaxe:ppdark=true/[false] Si coché, vous devrez spécifier le chemin de la bibliothèque de masterbiases et la convention de nommage des masterbiases Si coché, vous devrez spécifier le chemin de la bibliothèque de masterdarkflats et la convention de nommage des masterdarkflats Si coché, vous devrez spécifier le chemin de la bibliothèque de masterdarks et la convention de nommage des masterdarks Si coché, vous devrez spécifier le chemin de la bibliothèque de masterflats et la convention de nommage des masterflats Si plusieurs ensembles de lights peuvent coexister dans le même répertoire de light, fourni la convention de renommage de chaque ensemble
Example: "LIGHT_F[FILTER:s]_[EXPTIME:d]s_G[GAIN:d]_bin[XBINNING:d]" sans extension.

loop.cfg syntaxe:lightsfolders="" Si valide, l'option est mise à jour Session interactive annulée Charger Charger un fichier light pour visualiser ses champs FITS Chargement d'un fichier .cfg existant Master {0:s} trouvé @ {1:s} Nom du répertoire des biases Nom du répertoire des darkflats Nom du répertoire des darks Nom du répertoire des flats Nom du répertoire des lights La nouvelle version du master {0:s} {1:s} à été copié vers {2:s} Aucune commande à SSFier à partir de ce log - abandon Aucun répertoire {:s} ne correspond à la date Aucun n'a été trouvé - abandon Aucun n'a été trouvé - abandon Note: les arguments entre crochets sont optionels
 Ooops... Numéro d'option invalide - réessayez Appliquer les options à gess et procéder au traitement de la session Effectue l'alignement Effectuer l'empilement Veuillez sélectionner le répertoire de travail de la session Options de prétraitement Préférences Prétraitement des flats avec des offsets (bias ou darkflats) Prétraitement des lights Prétraitement des lights avec des darks Prétraitement des lights avec des flats Appuyez sur Entrée sur une ligne vide quand vous avez fini Affichage Options d'affichage Traitement Options de traitement Revoyez votre nomenclature - Utilisation du premier trouvé Mettre toutes les options à leur valeur par défaut Enregistrer Sous Enregistrement en tant que fichier cfg optionel Enregistrement du fichier de configuration sous...  Enregistrement des logs (Fortement recommandé) Recherche de tous les répertoires sous {0:s} contenant {1:s} Recherche de tous les répertoires sous {0:s} contenant la date {1:s} Recherche des sous-répertoires {:s} Sélectionnez un autre répertoire Selectionnez les répertoires contenant les lights calibrées - Annuler quand terminé Sélectionnez/créez le répertoire où stoker le résultat multisession Couches Ha et OIII séparées Numéro d'ensemble invalide - réessayez Emplacement de Siril Omission du traitement de ce répertoire Champ dédié aux utilisateurs de NINA. Lorsque rempli, il indique à gessloop que les flats peuvent être stocké ailleurs qu'avec les lights et comment les assigner correctement à chaque light.
Exemple: "FLAT_[FILTER:s]_O[OFFSET:d]_bin[XBINNING:d]" sans extension.

loop.cfg syntaxe:NINAflatwizard=""
Note: C'est assez similaire à utiliser une bibliothèque de flats à l'exception du fait que les masterflats ne seront pas persistants entre vos sessions La commande stack a échoué pour les {:s} Lancement de Siril Lancement de PySiril Succès Tester Tester mes options Le chemin spécifié pour la bibliothèque de master n'exister pas, verifiez et réessayez Il y a eu un problème lors de la copie des fichiers {:s} Il y a eu un problème lors de la copie des fichiers {:s} - vérifiez dans le log Cette option n'existe pas Cette option n'existe pas - elle est ignorée Tapez le nom de l'option et la nouvelle valeur, séparés par un signe = et appuyez sur Enrée Mode inconnu pour parsemasterformat Enregistrer les valeurs par défaut Mettre à jour le ficher cfg par défaut de Gess Valider Avertissement Bienvenue sur GeSS - vous trouverez ci-dessous un guide des différentes options et leurs arguments: Que voulez vous faire ensuite? Entrez le numéro d'une des options ci-dessous Si coché, le masteroffset sera soustrait des flats avant l'empilement
Si vous n'utilisez pas de bibliothèque d'offsets, les offsets seront empilés avant le prétraitement des flats

gess.cfg syntaxe:ppoffset=true/[false]
Note: la valeur de ppoffset est déterminée par la valeur de ppflat, la valeur de cette case à cocher ne peut être changée Vous être sur le point d'effacer toutes les champs de cette page.
Voulez-vous continuer? Vous êtes sur le point d'enregistrer toutes les valeurs en tant que nouvelles options par défaut
Voulez-vous continuer? Vous pouvez changer chacune des options ci-dessus Vous ne pouvez pas sélectionner un des répertoires source pour stocker le résultat multisession Vous avez réussi!
Voulez vous sauvegarder ces options? La commande d'extraction du fond de ciel a échoué pour la séquence {:s} gess -e workdir [addcfgfile=addgess.cfg] [opt1=val1] [opt2=val2]-> lance le moteur gess engine dans le répertoire de travail workdir
les options sont considérées dans cet ordre de priorité: options de la ligne de commande définies par leur nom et une valeur séparés par '=', options de addgess.cfg
 gess -i -> lance gess en mode interactif
Vous pourrez revoir/modifier les options et ensuite sélectionner le répertoire de travail
 gess -l -> lance gess en mode loop
gess va parcourir les sessions des nuits précédents
Requiert un fichier loop.cfg
 gess -l [addgesscfg=addgess.cfg] [addloopcfg=addloop.cfg] [searchstr=2021-01-15]-> lance gess en mode loop
gess va parcourir toutes les sessions d'aquisition à la date searchstr
Les options fournies par addgess.cfg et addloop.cfg écraseront celles de gess.cfg et loop.cfg respectivement
 gess -m -> lance gess en mode multisession
Sélectionnez plusieurs répertoires contenant des lights callibrées et elles seront regroupées, alignées et empilées
 gess -o -> lance l'assistant d'option de GeSS
Fournir un ensemble d'option correct est nécessaire au bon fonctionnement des autres mode de lancement
Vous pouvez aussi utiliser ce mode lancement pour définir d'autres ensemble d'option si vous avez plusieurs set-up etc...
 gess -s permet de "SSFyer" le log d'une session gess. Ce module extrait toutes les commandes ayant réussi et qui ont été passées à Siril.
Utile pour modifier et relancer Siril, ou simplement pour supprimer les messages verbeux et revoir les actions qui ont été effectuées
 option par défaut de gess depuis {:s} gess multisession a échoué gess.cfg doit spécifier le format du nom des masters {:s} gess.cfg doit spécifier le chemin de la bibliothèque des {:s} levelup ne peut être négatif ou nul - abandon loop.cfg doit spécifier une chaîne de caractère simple, non pas un chemin, indiquant où se trouvent les {:s} La commande register a échoué pour la séquence {:s} La commande seqextract_HaOIII a échoué pour la séquence {:s} La commande setcompress a échoué avec les arguments {:s} ssf sauvegardé dans {:s} La commande stack a échoué pour la séquence {:s} avec les options additionnelles de {:s} il n'a pas été possible d'empiler {0:s} à partir de {1:s} - vérifiez le log {:s} n'existe pas {:s} contiens des espaces et ne peut pas être utilisé dans les scripts {:s} n'est pas une option gess valide 